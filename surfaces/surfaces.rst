.. _surfaces:

Benchmark surface energies with RPA
===================================

See here: :ref:`adsorption`.


Key-value pairs
---------------

.. csv-table::
    :file: keytable.csv
    :header-rows: 1
    :widths: 3 10 1
