from __future__ import print_function
import csv
import os.path as op

from ase.db import connect

from downloads import downloads

for dir, names in downloads:
    for name in names:
        if name.endswith('.db'):
            if 0:
                # print('create database {};'.format(name[:-3]))
                print('{!r}, '.format(name[:-3]), end='')
            else:
                print(dir, name)
            #db = connect(op.join(dir, name))
            md = op.join(dir, 'metadata.py')
            if op.isfile(md):
                namespace = {}
                exec(open(md).read(), namespace)
                md = namespace['metadata']
                k = op.join(dir, 'keys.csv')
                kd = []
                if op.isfile(k):
                    with open(k) as fd:
                        for row in csv.reader(fd):
                            kd.append((row[0], [c.strip() for c in row[1:]]))
                #db.metadata = md
                print(dir, name, md)
                with open(op.join(dir, 'custom.py'), 'w') as fd:
                    print('title = {!r}'.format(md['title']), file=fd)
                    if kd:
                        print('\nkey_descriptions = {', end='', file=fd)
                        comma = False
                        for k, v in kd:
                            print(dir, k, v)
                            s, l, t, u = v
                            if l == s:
                                l = ''
                            if comma:
                                print(',', end='', file=fd)
                            comma = True
                            print('\n    {!r}: ({!r}, {!r}, {!r})'.format(
                                      k, s, l, u), end='', file=fd)
                        print('}', file=fd)
                    print('\ndefault_columns = {!r}'.format(md['default_columns']),
                          file=fd)
            name2 = 'postgresql://ase:ase@localhost:5432/{}'.format(name[:-3])
            if 0:  # connect(name2).count() == 0:
                with connect(name2) as db2:
                    for row in db.select():
                        kvp = row.key_value_pairs
                        db2.write(row, data=row.get('data'), **kvp)

            # connect(name2).metadata = db.metadata
