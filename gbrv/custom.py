title = 'Pseudopotentials for high-throughput DFT calculations'

default_columns = ['id', 'age', 'formula', 'energy', 'pbc', 'volume', 'charge', 'mass', 'category', 'name']
