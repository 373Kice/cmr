import matplotlib.pyplot as plt
from ase.db.web import creates

title = 'Database of ABS3 materials'

key_descriptions = {
    'E_hull': ('E-hull',
               'Energy of the convex hull '
               '(with respect to the most stable structure)', 'eV'),
    'm_e': ('Electron mass', 'Effective electron mass', '`m_e`'),
    'm_h': ('Hole mass', 'Effective hole mass', '`m_e`'),
    'E_relative_per_atom': ('Energy per atom',
                            'Energy per atom '
                            '(with respect to the most stable structure)',
                            'eV'),
    'E_uncertainty_hull': ('E-hull-unc',
                           'Uncertainty of the convex hull energy', 'eV'),
    'E_uncertainty_per_atom': ('E-unc', 'Uncertainty of the total energy',
                               'eV'),
    'GLLB_dir': ('GLLB-gap-dir', 'Direct band gap (GLLB-SC)', 'eV'),
    'GLLB_ind': ('GLLB-gap-ind', 'Indirect band gap (GLLB-SC)', 'eV'),
    'PBEsol_gap': ('PBEsol_gap', 'Band gap (PBEsol)', 'eV'),
    'lattice': ('Crystal system', 'Crystal system', ''),
    'prototype': ('Prototype', 'prototype name', ''),
    'ABS3_name': ('Short name', 'Short chemical formula', ''),
    'isreference': ('Is reference', '', '')}


special_keys = [
    ('SELECT', 'prototype')]

default_columns = ['formula', 'energy',
                   'ABS3_name', 'prototype', 'GLLB_ind', 'GLLB_dir',
                   'E_relative_per_atom']

basic = ('Item', ['prototype', 'GLLB_ind', 'GLLB_dir', 'm_e', 'm_h'])

layout = [('Basic properties',
           [[basic, 'CELL'],
            ['ATOMS']]),
          ('Electronic band structure',
           [['bs.png']])]


@creates('bs.png')
def bs(row):
    if not hasattr(row, 'data'):
        return
    d = row.data
    if 'x' in d:
        plt.plot(d.x, d.y)
        plt.xticks(d.X, d.names)
        plt.xlim(0, d.X[-1])
        plt.ylim(-6, 5)
        plt.ylabel('GLLB-SC energy relative to VBM [eV]')
        plt.savefig('bs.png')
        plt.close()
