from ase.db.web import creates

title = 'One and two component references from OQMD'

key_descriptions = {
    'de': ('Heat of formation', '', 'eV/atom'),
    'ns': ('Species', 'Number of species', ''),
    'emag': ('Mag. energy', 'Magnetization energy', 'eV/atom'),
    'u': ('PBE+U?', '', '')}

default_columns = ['formula', 'energy', 'magmom', 'de', 'ns', 'volume',
                   'fmax', 'smax', 'u']

special_keys = [
    ('SELECT', 'ns'),
    ('SELECT', 'u')]

basic = ('Property',
         default_columns +
         ['oqmd_' + key for key in
          ['prototype', 'bandgap', 'energy_pa', 'stability', 'delta_e']])

layout = [('Basic properties',
           [[basic, 'CELL'],
            ['ATOMS']]),
          ('Convex hull',
           [['convex-hull.png'], ['convex-hull.csv']]),
          ('Convex hull (with FERE)',
           [['convex-hull-2.png'], ['convex-hull-2.csv']])]


@creates('convex-hull.png', 'convex-hull.csv')
def convex_hull1(row):
    if 'hull' not in row.data:
        return
    hull = row.data.hull
    convex_hull(hull.x, hull.y, hull.formulas, hull.lines, hull.symbols,
                hull.ids, row.id,
                'convex-hull.png', 'convex-hull.csv')


@creates('convex-hull-2.png', 'convex-hull-2.csv')
def convex_hull2(row):
    if 'hull2' not in row.data:
        return
    hull = row.data.hull2
    convex_hull(hull.x, hull.y, hull.formulas, hull.lines, hull.symbols,
                hull.ids, row.id,
                'convex-hull-2.png', 'convex-hull-2.csv')


def convex_hull(x, y, formulas, lines, symbols, ids, row_id, pngname, csvname):
    import matplotlib.pyplot as plt
    fig = plt.figure()
    ax = fig.gca()
    for i, j in lines:
        ax.plot(x[[i, j]], y[[i, j]], '-', color='lightblue')
    ax.plot(x, y, 'og')
    j = list(ids).index(row_id)
    ax.plot([x[j]], [y[j]], 'sr')
    for a, b, name in zip(x, y, formulas):
        if a > 0.5:
            ax.text(a + 0.015, b, name, ha='left', va='top')
        else:
            ax.text(a - 0.015, b, name, ha='right', va='top')
    A, B = symbols
    ax.set_xlabel('{}$_{{1-x}}${}$_x$'.format(A, B))
    ax.set_ylabel('$\Delta H$ [eV/atom]')

    plt.tight_layout()
    plt.savefig(pngname)
    plt.close()

    with open(csvname, 'w') as f:
        f.write('# Formation energies\n')
        for a, e, formula, id in sorted(zip(x, y, formulas, ids)):
            if id != row_id:
                formula = ('<a href="/id/{}?project=oqmd12">{}</a>'
                           .format(id, formula))
            f.write('{}, {:.3f}, eV/atom\n'.format(formula, e))
