from collections import defaultdict

from ase.db import connect
from ase.phasediagram import PhaseDiagram
from c2db.fere import fere

nm = connect('nm.db')
fm = connect('fm.db')
nmu = connect('nmu.db')
fmu = connect('fmu.db')

db1 = connect('oqmd-1.db')
db2 = connect('oqmd-2.db')

db = connect('oqmd12.db')

u_atoms = {'Ti', 'V', 'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', 'Zn', 'Nb', 'Mo',
           'Tc', 'Ru', 'Rh', 'Pd', 'Cd', 'Ta', 'W', 'Re', 'Os', 'Ir', 'Pt'}
mag_elements = {'Sc', 'Ti', 'V', 'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', 'Zn',
                'Y', 'Zr', 'Nb', 'Mo', 'Ru', 'Rh', 'Pd', 'Ag', 'Cd', 'In',
                'Hf', 'Ta', 'W', 'Re', 'Os', 'Ir', 'Pt', 'Au', 'Hg', 'Tl'}


def collect(db, nm, fm, u=False, refs=None, formula2id=None):
    refs = refs or {}
    rows1 = []
    for row in nm.select():
        emag = None
        if row.natoms == 0:
            print(row.name, 'nm')
            continue
        if any(symbol in mag_elements for symbol in row.symbols):
            try:
                mrow = fm.get(formula=row.formula)
            except KeyError:
                print(row.formula, 'fm')
                continue
            if row.get('failed'):
                if mrow.get('failed'):
                    print(row.formula, 'nm+fm')
                    continue
                row = mrow
            else:
                if not mrow.get('failed'):
                    magnetic = (abs(mrow.get('magmom', 0.0)) > 0.05 and
                                mrow.energy < row.energy)
                    if magnetic:
                        emag = (row.energy - mrow.energy) / row.natoms
                        row = mrow
        else:
            if row.get('failed'):
                print(row.formula, 'nm')
                continue

        count = row.count_atoms()
        ns = len(count)
        if ns == 1:
            refs[row.symbols[0]] = row.energy / row.natoms
        rows1.append((ns, count, emag, row))

    rows2 = []
    combinations = defaultdict(list)
    for ns, count, emag, row in rows1:
        de = (row.energy -
              sum(n * refs.get(symbol, float('nan'))
                  for symbol, n in count.items())) / row.natoms
        ab = tuple(sorted(count))
        if ns == 2:
            combinations[ab].append((row.formula, de * row.natoms))
        rows2.append((de, ns, emag, row, ab))

    hulls = {}
    hulls2 = {}
    for ab, abrefs in combinations.items():
        abrefs = abrefs + [(x, 0.0) for x in ab]
        try:
            pd = PhaseDiagram(abrefs, verbose=False)
        except ValueError:
            print(ab, abrefs)
            continue
        hulls[ab] = ([x for x, e in abrefs],
                     pd.points, pd.hull, pd.simplices, pd.symbols)
        if not u:
            abrefs = []
            for count, energy, name, natoms in pd.references:
                if len(count) == 2:
                    energy -= sum(n * fere.get(symbol, 0.0)
                                  for symbol, n in count.items())
                abrefs.append((name, energy))
            pd = PhaseDiagram(abrefs, verbose=False)
            hulls2[ab] = ([x for x, e in abrefs],
                          pd.points, pd.hull, pd.simplices, pd.symbols)

    rows3 = sorted(rows2, key=lambda x: x[0])
    id = len(db)
    formula2id = formula2id or {}
    for de, ns, emag, row, ab in rows3:
        id += 1
        if ns == 1:
            formula2id[ab[0]] = id
        else:
            formula2id[row.formula] = id

    for de, ns, emag, row, ab in rows3:
        kvp = row.key_value_pairs
        if emag is not None:
            kvp['emag'] = emag
        if 1:
            if ns == 1:
                r = db1.get(formula=row.formula)
            else:
                r = db2.get(formula=row.formula)
            for k in ['prototype', 'bandgap', 'energy_pa',
                      'stability', 'delta_e']:
                kvp['oqmd_' + k] = r.get(k)
        if ns == 2:
            data = {}
            if ab in hulls:
                formulas, points, onhull, simplices, symbols = hulls[ab]
                data['hull'] = {
                    'formulas': formulas,
                    'ids': [formula2id[formula] for formula in formulas],
                    'x': points[:, 1],
                    'y': points[:, 2],
                    'onhull': onhull,
                    'lines': simplices,
                    'symbols': symbols}
            if not u:
                formulas, points, onhull, simplices, symbols = hulls2[ab]
                data['hull2'] = {
                    'formulas': formulas,
                    'ids': [formula2id[formula] for formula in formulas],
                    'x': points[:, 1],
                    'y': points[:, 2],
                    'onhull': onhull,
                    'lines': simplices,
                    'symbols': symbols}
        else:
            data = None
        db.write(row.toatoms(), ns=ns, de=de, u=u, data=data, **kvp)

    return refs, formula2id


with db:
    refs, formula2id = collect(db, nm, fm)
for symbol in u_atoms:
    refs.pop(symbol, None)
print('U' * 70)
collect(db, nmu, fmu, u=True, refs=refs, formula2id=formula2id)
