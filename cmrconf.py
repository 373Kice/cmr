# import subprocess

names = [
    'dssc',
    # 'molecules', 'solids',
    # 'dcdft', 'dcdft_gpaw_pw_paw09'
    'mp_gllbsc', 'organometal', 'cubic_perovskites',
    'low_symmetry_perovskites',
    'c2db',
    'tmfp06d', 'absorption_perovskites',
    'funct_perovskites', 'fcc111', 'compression', 'g2', 'catapp']

ASE_DB_NAMES = []
for db in names:
    # url = 'postgresql://ase:ase@localhost:5432/' + db
    url = db + '/' + db + '.db'
    py = db + '/custom.py'
    ASE_DB_NAMES.extend([url, py])
    # subprocess.check_call('python3 -m ase db {} -i all.db -k project={}'
    #                       .format(url, db), shell=True)
    # print('create database {};'.format(db))
    # print('ase db {} -i postgresql://ase:ase@localhost:5432/{}'
    #       .format(url, db))

ASE_DB_HOMEPAGE = 'https://cmr.fysik.dtu.dk/'
ASE_DB_FOOTER = """\
This work is licensed under a
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br />
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
<img alt="Creative Commons License" style="border-width:0"
src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>
"""
ASE_DB_TMPDIR = '/scratch/cmr2/static/'
ASE_DB_DOWNLOAD = False
