"""Topological determination of 2D 'covalent factor'

Author: Peter Larsen
Date: November 2017
"""

import numpy as np
from c2db import union_find as uf
from ase.neighborlist import NeighborList
from ase.data import covalent_radii


def generate_cell_relationships(natoms, extent, nl):

    unique_offsets = set()
    for i in range(natoms):
        indices, offsets = nl.get_neighbors(i)
        for n1, n2, n3 in offsets:
            unique_offsets.add((n1, n2, n3))

    rels = dict()
    nx, ny, nz = extent
    cells = np.array([(x, y, z) for x in range(nx)
                      for y in range(ny) for z in range(nz)])
    m = [1, nx, ny * nx]
    keys = np.dot(m, cells.T)
    for offset in unique_offsets:
        ds = []
        hit = set()
        nbr_cells = cells + offset
        for i in range(len(extent)):
            nbr_cells[:, i] %= extent[i]

        nbr_keys = np.dot(m, nbr_cells.T)
        for p0, p1, key0, key1 in zip(cells, nbr_cells, keys, nbr_keys):
            if key0 not in hit:
                ds += [(key0, key1)]
                hit.add(key0)
                hit.add(key1)
        rels[offset] = ds
    return rels


def generate_bonds(atoms, kmax):

    rs = covalent_radii[atoms.get_atomic_numbers()]
    nl = NeighborList(kmax * rs, skin=0, self_interaction=False)
    nl.update(atoms)

    bonds = []
    natoms = len(atoms)
    for i in range(natoms):
        p = atoms.positions[i]
        indices, offsets = nl.get_neighbors(i)

        for j, offset in zip(indices, offsets):
            q = atoms.positions[j] + np.dot(offset, atoms.get_cell())
            d = np.linalg.norm(p - q)
            bonds += [(d / (rs[i] + rs[j]), d, i, j, tuple(offset))]
    return sorted(bonds), nl


def minimum_covalent_factor(atoms):
    """Calculates the minimum 'covalent factor' needed for a material to
    be 2D.  The covalent factor, k, of a bond between atoms i and j is
    defined as k*(ri + rj) where ri and rj are the covalent radii.

    atoms: The atoms object.

    Returns
    =======

    k: float
        The minimum covalent factor.
        If the material is not 2D, -1 is returned.
    """

    natoms = len(atoms)
    extent = (2, 2, 2)
    num_cells = np.prod(extent)
    single_ranks = np.zeros(natoms).astype(np.int)
    single_parents = np.arange(natoms)
    super_ranks = np.zeros(natoms * num_cells).astype(np.int)
    super_parents = np.arange(natoms * num_cells)

    hit = set()
    kmax = 2
    while 1:

        bonds, nl = generate_bonds(atoms, kmax)
        rels = generate_cell_relationships(natoms, extent, nl)

        for (k, d, i, j, offset) in bonds:

            hit.add(i)
            hit.add(j)

            uf.find_and_merge(single_ranks, single_parents, i, j)

            for (c1, c2) in rels[offset]:
                uf.find_and_merge(super_ranks, super_parents,
                                  i + c1 * natoms, j + c2 * natoms)
                uf.find_and_merge(super_ranks, super_parents,
                                  i + c2 * natoms, j + c1 * natoms)

            single_parents = uf.compress(single_parents)
            super_parents = uf.compress(super_parents)

            # count num. components
            n0 = len(np.unique(single_parents))
            n1 = len(np.unique(super_parents))

            if len(hit) == natoms:

                if n0 >= 1 and n1 == 2 * n0:
                    return k

                if n1 == 1:
                    return -1

        kmax += 2
        if kmax >= 6:
            return -1


def is_2d(atoms):
    return minimum_covalent_factor(atoms) <= 1.3
