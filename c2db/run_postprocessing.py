import sys
import os
import numpy as np

from ase.db import connect


def add_experimental_bulk_parents(db, csvfile=None):
    import csv
    if csvfile is None:
        directory = os.path.dirname(__file__)
        filename = 'experimental_bulk_parents.csv'
    with open(os.path.join(directory, filename), 'r') as csvfile:
        csvfile.readline()  # skip header column
        reader = csv.reader(csvfile)
        for row in reader:
            uid, icsd_id, cod_id = row
            db_row = db.get(uid=uid)
            if icsd_id and not hasattr(db_row, 'ICSD_id'):
                db.update(db_row.id, ICSD_id=int(icsd_id))
            if cod_id and not hasattr(db_row, 'COD_id'):
                db.update(db_row.id, COD_id=int(cod_id))


def add_stability_level(db):

    def stability_levels(row):
        kvp = row.key_value_pairs
        mineig = kvp.get('minhessianeig', np.inf)
        try:
            elastic = min(kvp.get('c_11', None), kvp.get('c_22', None))
        except TypeError:
            elastic = None
        if mineig is None or elastic is None:
            dynamic_stability = None
        elif mineig < -2 or elastic < 0:
            dynamic_stability = 0
        elif mineig < -1e-5:
            dynamic_stability = 1
        else:
            dynamic_stability = 2
        hform = kvp.get('hform', None)
        ehull = kvp.get('ehull', None)
        if hform is None or ehull is None:
            thermodynamic_stability = None
        elif hform >= 0.2:
            thermodynamic_stability = 0
        elif ehull >= 0.2:
            thermodynamic_stability = 1
        else:
            thermodynamic_stability = 2
        return (thermodynamic_stability, dynamic_stability)

    for row in db.select():
        thermodynamic_stability, dynamic_stability = stability_levels(row)
        if dynamic_stability is not None:
            db.update(row.id, dynamic_stability_level=dynamic_stability)
        if thermodynamic_stability is not None:
            db.update(row.id,
                      thermodynamic_stability_level=thermodynamic_stability)


def main(db_file):
    from c2db.check_duplicates import delete_duplicates
    from c2db.crosslinks import crosslink
    from c2db.prototypes import update_prototypes
    from c2db.strip_data import strip
    from c2db.test import test
    db = connect(db_file)

    print('Setting correct prototypes...')
    update_prototypes(db)

    print('Deleting duplicates...')
    path, ext = os.path.splitext(db_file)
    duplicate_db = connect(path + '_duplicates' + ext)
    delete_duplicates(db, duplicate_db)

    print('Add experimental bulk references')
    add_experimental_bulk_parents(db)

    print('Setting stability level')
    add_stability_level(db)

    print('Create crosslinks')
    db2 = connect('c2db-big-2.db')
    with db2:
        crosslink(db, db2)

    print('Run tests')
    db3 = connect('c2db-big.db')
    with db3:
        test(db2, db3)

    print('Strip off data')
    db4 = connect('c2db.db')
    with db4:
        strip(db3, db4)


if __name__ == '__main__':
    main(sys.argv[1])
