import os
import traceback
from multiprocessing import Pool
from pathlib import Path

import matplotlib
from ase.db import connect
from ase.db.summary import Summary
from ase.db.web import process_metadata

from c2db.collect import collect
from c2db import readinfo, chdir
from c2db.run_postprocessing import main as postprocess

matplotlib.use('Agg')

folders = """\
TMHC
TMD
BN
C2
C3N
FeSe
MX2-1T
MX2-2H
MX3-1T
MX3-2H
MXenes/M2C
MXenes/M3C2
MoSSe
P4
PbSe
Perovskite
TMD_alloys
TMM
TiS3
WTe2
Xanes
#chalcohalides_workflow
#** not collected **:
#MX2-2H-afm
#TMD-afm"""

folders = [f for f in folders.splitlines() if f[0] != '#']

home = Path('/home/niflheim/mikst/C2DM-2018/')
refs = '/home/niflheim/jensj/cmr/oqmd12/oqmd12.db'


def collect_folder(folder):
    path = home / folder
    db = connect(path.name + '.db')
    err = open(path.name + '.err', 'w')
    for subfolder in path.glob('*-*/*m/'):
        print(subfolder)
        with chdir(subfolder):
            info = readinfo('..')
            # prototype = info.get('prototype')
            try:
                errors = collect(info, db, verbose=True,
                                 skip_forces=True,
                                 references=refs)
                for a, b, c in errors:
                    print(a, b, ''.join(c), file=err)
            except KeyboardInterrupt:
                break
            except Exception as x:
                tb = traceback.format_exc()
                err.write('{}: {}: {}\n'.format(subfolder, x, tb))
    err.close()


with Pool(processes=6) as pool:
    pool.map(collect_folder, folders)


# Combine:
db2 = connect('c2db-big-1.db')
for f in folders:
    path = home / f
    db = connect(path.name + '.db')
    with db2:
        for row in db.select():
            db2.write(row, data=row.get('data'), **row.key_value_pairs)

postprocess('c2db-big-1.db')

# Create png's:
db = connect('c2db-big.db')
db.python = '/home/niflheim/jensj/c2db-test/cmr/c2db/custom.py'
db.meta = process_metadata(db)
os.chdir('static')
for row in db.select():
    Summary(row, db.meta,
            prefix='c2db-{}-'.format(row.id),
            tmpdir='.').write()
