"""Workflow for C2DB project."""
import json
from pathlib import Path


def setup(tasks):
    state = Path().cwd().parts[-1]
    if state not in {'nm', 'fm', 'afm'}:
        if Path('info.json').is_file():
            # This folder has a info.json file.  Only one task to do:
            tasks.add('c2db.relax', '8x15h')
            # When this task has finished, the relevant subfolders
            # (nm, fm, afm) will exist.
        return

    # We are in a nm, fm or afm folder:
    gs = tasks.add('c2db.gs', '8x1h')
    tasks.add('c2db.phonons', '8x14h')

    # tasks.add('c2db.stability', '24x24h', deps=[gs])
    tasks.add('c2db.bandstructure', '8x3h', deps=[gs])
    tasks.add('c2db.polarizability', '24x10h', deps=[gs])
    tasks.add('c2db.plasmafrequency', '8x4h', deps=[gs])
    tasks.add('c2db.hse', '24x20h', deps=[gs])

    kpts = tasks.add('c2db.ehmasses', '8x3h', deps=[gs])

    tasks.add('c2db.em', '8x10h', deps=[kpts])
    tasks.add('c2db.strains', '8x6h', deps=[kpts])
    tasks.add('c2db.pdos', '8x1h', deps=[kpts])
    tasks.add('c2db.fermi_surface', '1x10m', deps=[kpts])

    if state == 'nm':
        tasks.add('c2db.gllbsc', '8x3h', deps=[kpts])
    else:
        tasks.add('c2db.anisotropy', '1x1h', deps=[kpts])

    if tasks.flags:
        natoms = json.loads(Path('../info.json').read_text())['natoms']
        if 'bse' in tasks.flags:
            cores = 24 * (natoms // 5 + 1)
            tasks.add('c2db.bse', deps=[gs], cores=cores, time='2d')
        if 'gw' in tasks.flags:
            cores = 24 * (natoms // 5 + 1)
            tasks.add('c2db.gw', deps=[gs], cores=cores, time='2d')
