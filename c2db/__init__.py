import argparse
import json
import os
import sys
import errno
from contextlib import contextmanager

import numpy as np
from ase.parallel import parprint, paropen, world
from ase.utils import devnull


def ensure_dir(folder):
    try:
        os.makedirs(folder)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise


def readinfo(dir='.'):
    if os.path.isfile(dir + '/info.json'):
        with open(dir + '/info.json') as fd:
            return json.load(fd)
    return {}


def writeinfo(dct):
    with paropen('info.json', 'w') as f:
        json.dump(dct, f, indent=True)


def update_info(**kwargs):
    if world.rank == 0:
        info = readinfo()
        info.update(**kwargs)
        writeinfo(info)


mag_elements = {'Sc', 'Ti', 'V', 'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', 'Zn',
                'Y', 'Zr', 'Nb', 'Mo', 'Ru', 'Rh', 'Pd', 'Ag', 'Cd', 'In',
                'Hf', 'Ta', 'W', 'Re', 'Os', 'Ir', 'Pt', 'Au', 'Hg', 'Tl'}


def magnetic_atoms(atoms):
    return np.array([symbol in mag_elements
                     for symbol in atoms.get_chemical_symbols()],
                    dtype=bool)


@contextmanager
def chdir(folder):
    dir = os.getcwd()
    os.chdir(str(folder))
    yield
    os.chdir(dir)


@contextmanager
def silence():
    sys.stdout = devnull
    yield
    sys.stdout = sys.__stdout__


@contextmanager
def cleanup(*files):
    try:
        yield
    finally:
        world.barrier()
        if world.rank == 0:
            for f in files:
                if os.path.isfile(f):
                    os.remove(f)


def make_parser(func):
    parser = argparse.ArgumentParser(description=func.__doc__)
    parser.add_argument('-v', '--verbose', action='store_true')
    parser.add_argument('folder', nargs='*', default=['.'])
    return parser


def run(func, *args):
    parser = make_parser(func)
    args2 = parser.parse_args()
    for folder in args2.folder:
        if args2.verbose:
            parprint(folder, end=' ')
        with chdir(folder):
            func(*args)
    if args2.verbose:
        parprint()
