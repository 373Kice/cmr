from __future__ import print_function
import numpy as np
import spglib
from ase.db import connect
from ase.data import covalent_radii
from c2db.distance_matrix import get_stoichiometry
from c2db.dimensionality import minimum_covalent_factor
from sklearn.cluster import DBSCAN


def get_reduced_stoichiometry(mystring):
    import re
    from functools import reduce
    from fractions import gcd
    split = re.findall('[A-Z][^A-Z]*', mystring)
    matches = [re.match('([^0-9]*)([0-9]+)', x)
               for x in split]
    numbers = [int(x.group(2)) if x else 1 for x in matches]
    symbols = [matches[i].group(1) if matches[i] else split[i]
               for i in range(len(matches))]
    divisor = reduce(gcd, numbers)
    if divisor == 1:
        return mystring

    result = ''
    numbers = [x // divisor for x in numbers]
    numbers = [str(x) if x != 1 else '' for x in numbers]
    for symbol, number in zip(symbols, numbers):
        result += symbol + number
    return result


def get_thickness(row):
    atoms = row.toatoms()
    radius = max([covalent_radii[x] for x in atoms.numbers])
    distance = atoms.positions[:, 2].max() - atoms.positions[:, 2].min()
    return distance / (2 * radius)


exceptions = {('AB3', 'P321'): ('AB3', 'P-62m'),
              ('AB3C8', 'Pm'): ('AB3C8', 'Pmm2'),
              ('A', 'P-3m1'): ('A', 'P6/mmm'),
              ('AB3', 'P-31m'): ('AB3', 'P-3m1'),
              ('AB3', 'Pc'): ('AB3', 'Pmmn'),
              ('AB3', 'P2_1'): ('AB3', 'P2_1/m'),
              ('AB2', 'P2/m'): ('AB2', 'C2/m'),
              ('ABC4', u'Pma2'): ('ABC4', u'Pmm2'),
              ('AB3', 'Pmn2_1'): ('AB3', 'Pmmn')}


def get_prototype_information(db,
                              symprec=0.5,
                              covalent_threshold=1.3, thickness_threshold=0.5):
    rows_2d = [row for row in db.select()
               if minimum_covalent_factor(row.toatoms()) <= covalent_threshold]
    rejected = [row for row in db.select()
                if minimum_covalent_factor(row.toatoms()) > covalent_threshold]

    prototype_labels = {('A', 'P6/mmm'): 'C',
                        ('AB', 'P-3m1'): 'CH',
                        ('AB', 'P3m1'): 'GeSe',
                        ('AB', 'P-6m2', 0): 'BN',
                        ('AB', 'P-6m2', 1): 'GaS',
                        ('AB', 'P4/nmm'): 'FeSe',
                        ('AB', 'Pmn2_1'): 'SnS',
                        ('AB', 'P4/mmm'): 'PbSe',
                        ('ABC', 'P3m1'): 'MoSSe',
                        ('ABC2', 'P3m1'): 'CH2Si',
                        ('ABC', 'Pmmn'): 'FeOCl',
                        ('AB2', 'P-6m2'): 'MoS2',
                        ('AB2', 'P-3m1'): 'CdI2',
                        ('AB2', 'P2_1/m'): 'WTe2',
                        ('AB3', 'P6/mmm', 0): 'C3N',
                        ('AB3', 'P-3m1'): 'BiI3',
                        ('AB3', 'Pmmn'): 'TiS3',
                        ('AB3', 'P-62m'): 'TiCl3',
                        ('A2B3', 'P-6m2'): 'Ti3C2',
                        ('AB2C2', 'P-3m1'): 'Ti2CO2',
                        ('A2B2C3', 'P-6m2'): 'Ti3C2O2',
                        ('AB2C2D2', 'P-3m1'): 'Ti2CH2O2',
                        ('A2B2C2D3', 'P-6m2'): 'Ti3C2H2O2',
                        ('Perovskite', 'N/A'): 'PbA2I4',
                        ('Undefined', 'N/A'): 'Undefined'}

    # We start by splitting every material in the database according to
    # stoichiometry and symmetry:
    tree = {}
    tree['Perovskite'] = {}
    tree['Perovskite']['N/A'] = []
    tree['Undefined'] = {}
    tree['Undefined']['N/A'] = []
    for row in rows_2d:
        atoms = row.toatoms()
        stoichiometry = get_stoichiometry(row.formula)
        sg = spglib.get_spacegroup(atoms, symprec=symprec).split()[0]
        if stoichiometry not in tree:
            tree[stoichiometry] = {}
        current_symmetries = (tree[stoichiometry][sg]
                              if sg in tree[stoichiometry] else [])
        current_symmetries.append(row)
        tree[stoichiometry][sg] = current_symmetries

    # There is still potentially a thin/thick case which needs to be taken care
    # of, so we analyse the thickness of each layer and split according to a
    # cluster model. Since this clustering is in a 1D space, it's easy to do.
    thickness_threshold = 0.5
    for stoichiometry in tree:
        if stoichiometry == "Perovskite" or stoichiometry == "Undefined":
            continue
        for sg in tree[stoichiometry]:
            rows = tree[stoichiometry][sg]
            thicknesses = np.array([get_thickness(row) for row in rows])
            model = DBSCAN().fit(thicknesses.reshape(-1, 1))
            labels = model.labels_
            unique_labels = list(set(labels) - set([-1]))
            n_clusters = len(unique_labels)
            if n_clusters <= 1:
                labels = [(stoichiometry, sg)]
                row_list = [rows]
            else:
                thick = [rows[i] for i in range(len(rows))
                         if thicknesses[i] >= thickness_threshold]
                thin = [rows[i] for i in range(len(rows))
                        if thicknesses[i] < thickness_threshold]
                tree[stoichiometry][sg] = {}
                tree[stoichiometry][sg][1] = thick
                tree[stoichiometry][sg][0] = thin
                row_list = [thin, thick]
                labels = [(stoichiometry, sg, 0), (stoichiometry, sg, 1)]
            for label, rows in zip(labels,
                                   row_list):
                if label not in prototype_labels and label not in exceptions:
                    ehulls = np.array([x.get('ehull', np.inf)
                                       for x in rows])
                    characteristic_row = rows[ehulls.argmin()]
                    f = characteristic_row.formula
                    prototype_labels[label] = get_reduced_stoichiometry(f)

    # Finally, we assign a prototype even to those which have fallen apart: If
    # they fit into an existing prototype, we use that. Otherwise, "Undefined"
    # is chosen. Additionally, perovskites are not analysed for symmetry
    for row in rejected:
        atoms = row.toatoms()
        stoichiometry = get_stoichiometry(row.formula)
        sg = spglib.get_spacegroup(atoms, symprec=symprec).split()[0]
        if len(atoms) >= 50:
            tree['Perovskite']['N/A'].append(row)
        elif stoichiometry not in tree or sg not in tree[stoichiometry]:
            tree['Undefined']['N/A'].append(row)
        elif not isinstance(tree[stoichiometry][sg], dict):
            tree[stoichiometry][sg].append(row)
        elif get_thickness(atoms) < thickness_threshold:
            tree[stoichiometry][sg][0].append(row)
        else:
            tree[stoichiometry][sg][0].append(row)
    return tree, prototype_labels


def update_prototypes(db):
    tree, prototype_labels = get_prototype_information(db)
    for stoichiometry in tree:
        for sg in tree[stoichiometry]:
            index = (stoichiometry, sg)
            if index in exceptions:
                index = exceptions[index]
            if not isinstance(tree[stoichiometry][sg], dict):
                rows = tree[stoichiometry][sg]
                labels = [prototype_labels[index]] * len(rows)
            else:
                rows = tree[stoichiometry][sg][0]
                l1 = len(rows)
                rows += tree[stoichiometry][sg][1]
                l2 = len(rows) - l1
                labels = ([prototype_labels[index + (0,)]] * l1 +
                          [prototype_labels[index + (1,)]] * l2)
            for row, label in zip(rows, labels):
                uid = '-'.join([row.formula, label, row.magstate])
                if uid != getattr(row, 'uid', None):
                    db.update(row.id, prototype=label, uid=uid)


if __name__ == "__main__":
    import sys
    try:
        db_file = sys.argv[1]
    except IndexError:
        db_file = '/home/niflheim/sthaa/C2DB/c2db.db'
    db = connect(db_file)
    update_prototypes(db)
