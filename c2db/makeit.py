"""
folders.txt should contain the folders that contains folders of type
formula-prototype/magstate/

run it like (clearing the first time):
makeit.py -c &
makeit.py &
makeit.py &
...
"""

import os
from pathlib import Path
from c2db import chdir


def get_folders():
    with open('folders.txt', 'r') as fd:
        lines = fd.readlines()
    lines2 = []
    for l in lines:
        l2 = l.rstrip()
        if '#' in l2 and l.lstrip()[0] == '#':
            continue
        if len(l2) > 0:
            lines2.append(l2)
    return lines2


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='make C2DB great again')
    # parser.add_argument('folder', nargs='+')
    parser.add_argument('-c', '--clear', action='store_true')
    args = parser.parse_args()

    folders = get_folders()
    if args.clear:
        for folder in folders:
            for x in ['.done', '.lockdown']:
                xname = '../{}/{}'.format(folder, x)
                try:
                    if Path(xname).is_file():
                        os.system('rm ' + xname)
                except Exception as x:
                    pass
        quit()
    for folder in folders:
        with chdir('../{}'.format(folder)):
            if Path('.lockdown').is_file() or Path('.done').is_file():
                continue
            os.system('touch .lockdown')
            try:
                print(folder)
                p0 = Path('c2db.db')
                p1 = Path('c2db_lw.db')
                p2 = Path('collect.out')
                for p in [p0, p1, p2]:
                    if p.is_file():
                        fname = p.name
                        fname2 = fname + '-old'
                        print('renaming {} to {}'.format(fname, fname2))
                        p.rename(fname2)
                os.system('python -m c2db.collect *-*/*/ -s > collect.out')
                os.system('ase db c2db.db -i c2db_lw.db --strip-data')
                os.system('touch .done')
            except KeyboardInterrupt as x:
                break
            except Exception as x:
                print(folder, x)
            finally:
                os.system('rm .lockdown')
