"""Postprocessing for db.

Find all rows of same stoichiometry.
"""

from collections import defaultdict

from ase.db import connect

from c2db.references import fenergy


def crosslink(db1, db2):
    refs = defaultdict(list)
    formulas = {}
    for id, row in enumerate(db1.select(), 1):
        count = row.count_atoms()
        key = tuple(sorted(count))
        hform = fenergy(row.energy, count) / row.natoms
        refs[key].append((hform,
                          row.formula,
                          row.prototype,
                          row.magstate,
                          id))
        formulas[row.id] = key

    for systems in refs.values():
        systems.sort()

    print(len(db1), 'rows')
    print(len(refs), 'stoichiometries')

    for row in db1.select():
        try:
            data = row.data
        except AttributeError:
            data = {}
        key = formulas[row.id]
        data['references'] = refs[key][:]
        if len(key) == 3:
            for i, s1 in enumerate(key):
                for s2 in key[i + 1:]:
                    data['references'] += refs.get((s1, s2), [])
        kvp = row.key_value_pairs
        kvp['nkinds'] = len(key)  # move this to collect.py
        a = row.toatoms()
        a.calc.name = 'gpaw'  # move this to collect.py
        db2.write(a, data=data, **kvp)
        print('.', end='', flush=True)

    print()


if __name__ == '__main__':
    import sys
    db1 = connect(sys.argv[1])
    with connect(sys.argv[2]) as db2:
        crosslink(db1, db2)
