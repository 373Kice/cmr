import json
import os.path as op

import numpy as np

from ase.dft.bandgap import bandgap
from ase.io.jsonio import encode
from ase.parallel import paropen
from gpaw import GPAW, FermiDirac
from gpaw.spinorbit import get_spinorbit_eigenvalues
import gpaw.mpi as mpi

from c2db import silence
from c2db.utils import eigenvalues, fermi_level


def gllbsc():
    calc = GPAW('densk.gpw', txt=None)
    atoms = calc.get_atoms()
    if atoms.get_initial_magnetic_moments().any():
        return
    if not op.isfile('gllbsc.gpw') or not op.isfile('gllbsc.json'):
        dct = calc.todict()
        dct.update(fixdensity=False, txt='gllbsc.txt', xc='GLLBSC',
                   parallel={'band': 1},
                   occupations=FermiDirac(width=0.01))
        calc = GPAW(**dct)
        atoms.set_calculator(calc)
        atoms.get_potential_energy()
        response = atoms.calc.hamiltonian.xc.xcs['RESPONSE']
        response.calculate_delta_xc()
        Eks, deltaxc = response.calculate_delta_xc_perturbation()
        atoms.calc.write('gllbsc.gpw')
        v = atoms.calc.get_electrostatic_potential()
        evac = v[:, :, 0].mean()
        with paropen('gllbsc.json', 'w') as fd:
            json.dump({'deltaxc': deltaxc,
                       'evac': evac}, fd, indent=True)
        mpi.world.barrier()

    with paropen('gllbsc.json', 'r') as fd:
        dct = json.load(fd)

    deltaxc = dct['deltaxc']
    # write gaps
    ranks = [0]
    comm = mpi.world.new_communicator(ranks)
    if mpi.world.rank in ranks:
        with silence():
            calc = GPAW('gllbsc.gpw', communicator=comm, txt=None)
        eps_kn = eigenvalues(calc)[0]
        eps_kn[eps_kn > calc.get_fermi_level()] += deltaxc
        efermi_nosoc = calc.get_fermi_level()
        gap_nosoc, p1, p2 = bandgap(eigenvalues=eps_kn, efermi=efermi_nosoc,
                                    output=None)
        if gap_nosoc > 0:
            dct.update(vbm_gllbsc_nosoc=eps_kn[p1],
                       cbm_gllbsc_nosoc=eps_kn[p2])
        dir_gap_nosoc, p1, p2 = bandgap(eigenvalues=eps_kn,
                                        efermi=efermi_nosoc,
                                        direct=True, output=None)
        eps_km = get_spinorbit_eigenvalues(calc, gw_kn=eps_kn).transpose()
        efermi = fermi_level(calc, eps_km[np.newaxis],
                             nelectrons=2 * calc.get_number_of_electrons())
        gap, p1, p2 = bandgap(eigenvalues=eps_km, efermi=efermi, output=None)
        if gap > 0:
            dct.update(vbm_gllbsc=eps_km[p1],
                       cbm_gllbsc=eps_km[p2])
        dir_gap, p1, p2 = bandgap(eigenvalues=eps_km, efermi=efermi,
                                  direct=True, output=None)
        dct.update(gap_gllbsc=gap,
                   dir_gap_gllbsc=dir_gap,
                   gap_gllbsc_nosoc=gap_nosoc,
                   dir_gap_gllbsc_nosoc=dir_gap_nosoc)
        jdct = encode(dct)
        with paropen('gllbsc.json', 'w') as fd:
            fd.write(jdct)


if __name__ == '__main__':
    gllbsc()
