"""Workflow for C2DB project."""
from pathlib import Path
from myqueue.task import task


def create_tasks():
    folder = Path.cwd().name
    if folder not in ['nm', 'fm', 'afm']:
        return [task('c2db.relax@8:1d')]

    if Path('duplicate').is_file() or Path('stop').is_file():
        return []

    tasks = [task('c2db.phonons@8:14h', deps='../c2db.relax'),
             task('c2db.gs@8:1h', deps='../c2db.relax'),
             task('c2db.vacuumlevels@8:1h', deps='c2db.gs'),
             task('c2db.bandstructure@8:3h', deps='c2db.gs'),
             task('c2db.polarizability@24:10h', deps='c2db.gs'),
             task('c2db.plasmafrequency@8:4h', deps='c2db.gs'),
             task('c2db.hse@24:20h', deps='c2db.gs'),
             task('c2db.hseinterpol@8:10m', deps='c2db.hse'),
             task('c2db.ehmasses@8:3h', deps='c2db.gs'),
             task('c2db.em@8:10h', deps='c2db.ehmasses'),
             task('c2db.embzcut@8:10h', deps='c2db.em'),
             task('c2db.strains@8:6h', deps='c2db.ehmasses'),
             task('c2db.pdos@8:1h', deps='c2db.ehmasses'),
             task('c2db.emexciton@8:6h', deps='c2db.pdos'),
             task('c2db.fermi_surface@1:10m', deps='c2db.ehmasses')]

    if folder == 'nm':
        tasks.append(task('c2db.gllbsc@8:3h', deps='c2db.ehmasses'))
    else:
        tasks.append(task('c2db.anisotropy@1:1h', deps='c2db.ehmasses'))

    if 0:
        tasks.extend([task('c2db.gw@192:xeon24:2d', deps='c2db.gs'),
                      task('c2db.gwinterpol@8:10m', deps='c2db.gw')])

    return tasks
