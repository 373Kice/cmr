import os
import numpy as np
from gpaw import GPAW
from gpaw.response.g0w0 import G0W0
# from gpaw.response.g0w0_with_gwg import G0W0
import gpaw.mpi as mpi
from c2db.bsinterpol import interpolate_bandstructure as ip_bs
from c2db.utils import fermi_level
import pickle
from gpaw.spinorbit import get_spinorbit_eigenvalues as get_soc_eigs
from ase.dft.bandgap import bandgap


def get_bandrange(calc):
    """lower and upper band range
    """
    # bands (-8,4)
    lb, ub = max(calc.wfs.nvalence // 2 - 8, 0), calc.wfs.nvalence // 2 + 4
    return lb, ub


def gw_calc(kptdensity=5, ecut=200.0, gwg_and_gw=False, gw_only=True):
    """Calculate the gw bandstructure"""

    # get non-magnetic gs
    if not os.path.isfile('gs.gpw'):
        return
    else:
        calc = GPAW('gs.gpw', txt=None)

    # kpts
    def get_kpts_size(atoms, density):
        """trying to get a reasonable monkhorst size which hits high
        symmetry points
        """
        from gpaw.kpt_descriptor import kpts2sizeandoffsets as k2so
        size, offset = k2so(atoms=atoms, density=density)
        size[2] = 1
        for i in range(2):
            if size[i] % 6 != 0:
                size[i] = 6 * (size[i] // 6 + 1)
        kpts = {'size': size, 'gamma': True}
        return kpts

    kpts = get_kpts_size(atoms=calc.atoms, density=kptdensity)

    # gs with wf
    if not os.path.isfile('gs_gw.gpw'):
        calc.set(kpts=kpts,
                 fixdensity=True,
                 txt='gs_gw.txt')
        calc.get_potential_energy()
        calc.diagonalize_full_hamiltonian(ecut=ecut)
        calc.write('gs_gw_nowfs.gpw')
        calc.write('gs_gw.gpw', mode='all')
    # bands (-8,4)
    lb, ub = get_bandrange(calc)

    # gw...
    if gwg_and_gw:
        calc = G0W0(calc='gs_gw.gpw',
                    bands=(lb, ub),
                    ecut=ecut,
                    ecut_extrapolation=True,
                    xc='rALDA',
                    av_scheme='wavevector',
                    fxc_mode='GWG',
                    do_GW_too=True,
                    truncation='2D',
                    nblocksmax=True,
                    q0_correction=True,
                    filename='g0w0',
                    # restartfile='g0w0.tmp',
                    savepckl=True)
    elif gw_only:
        calc = G0W0(calc='gs_gw.gpw',
                    bands=(lb, ub),
                    ecut=ecut,
                    ecut_extrapolation=True,
                    truncation='2D',
                    nblocksmax=True,
                    q0_correction=True,
                    filename='g0w0',
                    restartfile='g0w0.tmp',
                    savepckl=True)

    calc.calculate()

    # gw_results(bandrange=list(range(lb, ub)))


def gw_results(bandrange=None, name=None):
    """get band structure (still not aligned to evac), to gw.npz"""
    if os.path.isfile('g0w0_results_GW.pckl'):
        gw_file = 'g0w0_results_GW.pckl'
    elif os.path.isfile('g0w0_results.pckl'):
        gw_file = 'g0w0_results.pckl'
    else:
        return
    if name is None:
        name = ''
    else:
        name = name + '_'
    ranks = [0]
    comm = mpi.world.new_communicator(ranks)
    if mpi.world.rank in ranks:
        if os.path.isfile('gs_gw_nowfs.gpw'):
            gpw = 'gs_gw_nowfs.gpw'
        else:
            gpw = 'gs_gw_grr_nowfs.gpw'
        calc = GPAW(gpw, communicator=comm, txt=None)
        if bandrange is None:
            bandrange = list(range(*get_bandrange(calc)))
        gw_skn = pickle.load(open(gw_file, 'rb'), encoding='latin1')['qp']
        e_mk = get_soc_eigs(calc, gw_kn=gw_skn, return_spin=False,
                            bands=bandrange)
        extra_data = dict(cell=calc.atoms.get_cell(),
                          e_mk=e_mk,
                          bandrange=bandrange,
                          bzk_kc=calc.get_bz_k_points(),
                          ibzk_kc=calc.get_ibz_k_points(),
                          bz2ibzmap=calc.get_bz_to_ibz_map())

        np.savez(name + 'gw_extra.npz', **extra_data)
        perm_mk = e_mk.argsort(axis=0)
        for e_m, perm_m in zip(e_mk.T, perm_mk.T):
            e_m[:] = e_m[perm_m]

        e_skm = e_mk.T[np.newaxis]
        efermi = fermi_level(calc, e_skm,
                             nelectrons=(2 * (calc.get_number_of_electrons() -
                                              bandrange[0] * 2)))

        gap, p1, p2 = bandgap(eigenvalues=e_skm, efermi=efermi,
                              output=None)
        gapd, p1d, p2d = bandgap(eigenvalues=e_skm, efermi=efermi,
                                 direct=True, output=None)

        if not gap > 0:
            return
        vbm = e_skm[p1]
        cbm = e_skm[p2]

        data = dict(gap_gw=gap,
                    bandrange=bandrange,
                    dir_gap_gw=gapd,
                    cbm_gw=cbm,
                    vbm_gw=vbm,
                    efermi=efermi)
        try:
            kpts, e_skm, xreal, epsreal_skn = ip_bs(calc, e_skn=e_skm,
                                                    npoints=400)
        except Exception as x:
            print(x)
        else:
            data.update(path=kpts, eps_skn=e_skm, xreal=xreal,
                        epsreal_skn=epsreal_skn)

        np.savez(name + 'gw.npz', **data)


def gw_check(window=[-42, 42]):
    if os.path.isfile('g0w0_results_GW.pckl'):
        gw_file = 'g0w0_results_GW.pckl'
    else:
        gw_file = 'g0w0_results.pckl'
    result = pickle.load(open(gw_file, 'rb'), encoding='bytes')

    minChi = 1.0

    ranks = [0]
    comm = mpi.world.new_communicator(ranks)
    if mpi.world.rank in ranks:
        calc = GPAW('gs_gw.gpw', communicator=comm, txt=None)
        lb, ub = get_bandrange(calc)
        bandrange = list(range(lb, ub))

        e_mk = get_soc_eigs(calc, gw_kn=result['qp'], return_spin=False,
                            bands=bandrange)

        for ik in range(0, result['sigr2_skn'].shape[1]):
            for ib in range(0, result['sigr2_skn'].shape[2]):
                if e_mk[ib][ik] >= window[0] and e_mk[ib][ik] <= window[1]:
                    m = min(result['sigr2_skn'][0][ik][ib],
                            result['dsigr2_skn'][0][ik][ib])
                    minChi = min(minChi, m)

    return minChi


if __name__ == '__main__':
    from c2db import cleanup
    with cleanup('gs_gw.gpw'):
        gw_calc()
