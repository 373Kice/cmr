import os
from gpaw.response.df import DielectricFunction
import numpy as np
from gpaw.mpi import world
from gpaw.occupations import FermiDirac

from gpaw import GPAW


def excitedstates(kptdensity=20):
    """Calculate excited states for polarizability calculation"""

    def get_kpts_size(atoms, density):
        """trying to get a reasonable monkhorst size which hits high
        symmetry points
        """
        from gpaw.kpt_descriptor import kpts2sizeandoffsets as k2so
        size, offset = k2so(atoms=atoms, density=density)
        size[2] = 1
        for i in range(2):
            if size[i] % 6 != 0:
                size[i] = 6 * (size[i] // 6 + 1)
        kpts = {'size': size, 'gamma': True}
        return kpts

    calc_old = GPAW('gs.gpw', txt=None)
    kpts = get_kpts_size(atoms=calc_old.atoms, density=kptdensity)
    nval = calc_old.wfs.nvalence
    calc = GPAW('gs.gpw',
                fixdensity=True,
                kpts=kpts,
                nbands=6 * nval,
                convergence={'bands': 5 * nval},
                txt='ex.txt',
                occupations=FermiDirac(width=1e-4))
    calc.get_potential_energy()
    calc.write('es.gpw', 'all')


def polarizability(ecut=50.0):
    """Calculate polarizability"""
    tetra = True
    if not os.path.isfile('es.gpw'):
        excitedstates()

    volume = GPAW('es.gpw', txt=None).atoms.get_volume()

    if volume < 120:
        nblocks = world.size // 4
    else:
        nblocks = world.size // 2

    if tetra:
        kwargs = {'truncation': '2D',
                  'eta': 0.05,
                  'domega0': 0.005,
                  'integrationmode': 'tetrahedron integration',
                  'ecut': ecut,
                  'intraband': False,
                  'nblocks': nblocks,
                  'pbc': [True, True, False],
                  'name': 'chi_tetra'}
    else:
        kwargs = {'truncation': '2D',
                  'eta': 0.05,
                  'domega0': 0.005,
                  'intraband': False,
                  'nblocks': nblocks,
                  'ecut': ecut,
                  'name': 'chi'}

    df = DielectricFunction('es.gpw', **kwargs)
    alpha0x, alphax = df.get_polarizability(q_c=[0, 0, 0],
                                            direction='x',
                                            pbc=[True, True, False],
                                            filename=None)
    alpha0y, alphay = df.get_polarizability(q_c=[0, 0, 0],
                                            direction='y',
                                            pbc=[True, True, False],
                                            filename=None)
    alpha0z, alphaz = df.get_polarizability(q_c=[0, 0, 0],
                                            direction='z',
                                            pbc=[True, True, False],
                                            filename=None)

    plasmafreq_vv = df.chi0.plasmafreq_vv

    frequencies = df.get_frequencies()
    data = {'alpha0x_w': np.array(alpha0x),
            'alphax_w': np.array(alphax),
            'alpha0y_w': np.array(alpha0y),
            'alphay_w': np.array(alphay),
            'alpha0z_w': np.array(alpha0z),
            'alphaz_w': np.array(alphaz),
            'plasmafreq_vv': plasmafreq_vv,
            'frequencies': frequencies}

    if tetra:
        filename = 'polarizability_tetra.npz'
    else:
        filename = 'polarizability.npz'

    if world.rank == 0:
        np.savez_compressed(filename, **data)


if __name__ == '__main__':
    from c2db import cleanup
    with cleanup('es.gpw', 'chi_tetra+0+0+0.pckl', 'chi+0+0+0.pckl'):
        polarizability()
