from collections import defaultdict
import json
import os.path as op

import numpy as np
from ase import Atoms
from ase.dft.dos import ltidos
from ase.io import jsonio
from ase.parallel import paropen
from ase.units import Ha
import gpaw.mpi as mpi
from gpaw import GPAW
from gpaw.utilities.dos import raw_orbital_LDOS, raw_spinorbit_orbital_LDOS

from c2db import magnetic_atoms


def count(zs):
    """Count the number of occurences in a list

    Parameters:
    zs: [z1, z2, ...]-list or ndarray
        list of int's or str's

    Returns:
    out: {z1: n1, ...}
        n1 is the number of occurences of z1

    Examples:
    >>> zs = [1, 8, 8]
    >>> count(zs)
    {8: 2, 1: 1}
    >>> zs = ['H', 'O', 'O']
    >>> count(zs)
    {'O': 2, 'H': 1}
    """
    c = defaultdict(int)
    for z in zs:
        c[z] += 1
    return c


def get_l_a(zs):
    """Defines which atoms and angular momentum to project onto.

    Parameters:
    zs: [z1, z2, ...]-list or array
        list of atomic numbers (zi: int)

    Returns:
    out: {int: str, ...}-dict
        keys are atomic indices and values are a string such as 'spd'
        that determines which angular momentum to project onto or a
        given atom
    """
    zs = np.asarray(zs)
    l_a = {}
    atoms = Atoms(numbers=zs)
    mag_elements = magnetic_atoms(atoms)
    for a, (z, mag) in enumerate(zip(zs, mag_elements)):
        l_a[a] = 'spd' if mag else 'sp'
    return l_a


def dft_for_pdos(kptdens=36.0):
    from c2db.ehmasses import nonsc
    calc = nonsc(kdens=kptdens, emptybands=20, outname='pdos')
    return calc


def run_pdos(calc='pdos.gpw'):
    if not op.isfile('pdos.gpw'):
        dft_for_pdos()
    dosef_nosoc = dosef()
    with paropen('dosef_nosoc.txt', 'w') as fd:
        print('{}'.format(dosef_nosoc), file=fd)
    pdos(calc, spinorbit=False)
    pdos(calc, spinorbit=True)


def pdos(gpwname, spinorbit=True):
    """
    Writes the projected dos to a file pdos.json or pdos_soc.json

    Parameters:
    calc: GPAW calculator object or str
        calculator with a method get_orbital_ldos
    spinorbit: bool
        spin orbit coupling
    """
    world = mpi.world
    calc = GPAW(gpwname, txt=None)
    if spinorbit and world.rank == 0:
        calc0 = GPAW(gpwname, communicator=mpi.serial_comm)

    zs = calc.atoms.get_atomic_numbers()
    chem_symbols = calc.atoms.get_chemical_symbols()
    efermi = calc.get_fermi_level()
    l_a = get_l_a(zs)
    kd = calc.wfs.kd

    if spinorbit:
        ldos = raw_spinorbit_orbital_LDOS
    else:
        ldos = raw_orbital_LDOS

    e = np.linspace(-10 + efermi, 10 + efermi, 2000)
    ns = calc.get_number_of_spins()
    pdos_sal = defaultdict(float)
    e_s = {}
    for spin in range(ns):
        for a in l_a:
            spec = chem_symbols[a]
            for l in l_a[a]:
                if spinorbit:
                    if world.rank == 0:
                        energies, weights = ldos(calc0, a, spin, l)
                        mpi.broadcast((energies, weights))
                    else:
                        energies, weights = mpi.broadcast(None)
                else:
                    energies, weights = ldos(calc, a, spin, l)

                energies.shape = (kd.nibzkpts, -1)
                energies = energies[kd.bz2ibz_k]
                energies.shape = tuple(kd.N_c) + (-1,)
                weights.shape = (kd.nibzkpts, -1)
                weights /= kd.weight_k[:, np.newaxis]
                w = weights[kd.bz2ibz_k]
                w.shape = tuple(kd.N_c) + (-1,)
                p = ltidos(calc.atoms.cell, energies * Ha, e, w)
                key = ','.join([str(spin), str(spec), str(l)])
                pdos_sal[key] += p
        e_s[spin] = e

    data = {'energies': e,
            'pdos_sal': pdos_sal,
            'symbols': calc.atoms.get_chemical_symbols(),
            'efermi': efermi}
    fname = 'pdos_soc.json' if spinorbit else 'pdos.json'
    with paropen(fname, 'w') as fd:
        json.dump(jsonio.encode(data), fd)


def dosef():
    """
    Get dos at ef
    """
    name = 'pdos.gpw' if op.isfile('pdos.gpw') else 'densk.gpw'
    calc = GPAW(name, txt=None)

    from ase.dft.dos import DOS
    dos = DOS(calc, width=0.0, window=(-0.1, 0.1), npts=3)
    return dos.get_dos()[1]


def plot_pdos():
    """only for testing
    """
    efermi = GPAW('gs.gpw', txt=None).get_fermi_level()
    import matplotlib.pyplot as plt
    with paropen('pdos.json', 'r') as fd:
        data = jsonio.decode(json.load(fd))
        e = np.asarray(data['energies'])
        pdos_sal = data['pdos_sal']
        symbols = data['symbols']

    with paropen('evac.txt', 'r') as fd:
        evac = float(fd.read())

    e -= evac
    pmax = 0.0
    for s, pdos_al in pdos_sal.items():
        for a, pdos_l in sorted(pdos_al.items()):
            for l, pdos in sorted(pdos_l.items(), reverse=True):
                pdos = np.asarray(pdos)
                pmax = max(pdos.max(), pmax)
                plt.plot(pdos, e, label='{} ({})'.format(symbols[int(a)], l))
    plt.xlim(0, pmax)
    plt.ylim(efermi - evac - 2, efermi - evac + 2)
    plt.legend()
    plt.ylabel('energy relative to vacuum [eV]')
    plt.xlabel('pdos [states/eV]')
    plt.show()


if __name__ == '__main__':
    run_pdos('pdos.gpw')
