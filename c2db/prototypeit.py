import argparse
from c2db import chdir, update_info, readinfo
import os


def get_prototype(name):
    return (name.split('-')[1]).split('/')[0]


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Create info.json files')
    parser.add_argument('folder', nargs='+')
    parser.add_argument('-v', '--verbose', action='store_true')
    parser.add_argument('-c', '--cls', default='')
    parser.add_argument('-p', '--prototype', default='')
    args = parser.parse_args()
    folders = args.folder
    for folder in folders:
        d = {'prototype': '', 'class': args.cls}
        if not os.path.isdir(folder):
            continue
        if args.prototype == '':
            prototype = get_prototype(folder)
        else:
            prototype = args.prototype
        with chdir(folder):
            d['prototype'] = prototype
            d2 = readinfo()
            if 'class' in d2 and d['class'] == '':
                del d['class']
            update_info(**d)
