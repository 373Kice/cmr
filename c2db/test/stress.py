from ase import Atoms
from ase.calculators.lj import LennardJones
from c2db.bfgs import BFGS
# Theoretical infinite-cutoff LJ FCC unit cell parameters
vol0 = 4 * 0.91615977036  # theoretical minimum
a0 = vol0**(1 / 3)

a = Atoms('X2', [(0, 0, 0), (0, 0, 1)], cell=[1, 1, 10], pbc=[1, 1, 0])
a.calc = LennardJones()
print(a.get_forces()[0, 2], a.get_stress()[0])

opt = BFGS(a, trajectory='x2.traj', logfile='x2.log')
opt.run(fmax=0.0001, smax=0.0001, smask=[1, 1, 0, 0, 0, 0])
print(a.get_forces()[0, 2], a.get_stress()[0])
