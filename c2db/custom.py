import sys
from collections import defaultdict, OrderedDict
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from ase import Atoms
from ase.utils import formula_metal, basestring
from ase.db.web import creates
from ase.dft.band_structure import BandStructure
from ase.dft.kpoints import labels_from_kpts
from ase.units import Ha, Bohr, alpha
from c2db.bz import plot_bz
import matplotlib.patheffects as path_effects

assert sys.version_info >= (3, 4)


title = 'Computational 2D materials database'

key_descriptions = {
    'prototype': ('Prototype', 'Structure prototype', ''),
    'class': ('Class', 'Class of material', ''),
    'ICSD_id': ('ICSD id', 'ICSD id of parent bulk structure', ''),
    'COD_id': ('COD id', 'COD id of parent bulk structure', ''),
    'has_invsymm': ('Inversion symmetry', '', ''),
    'magstate': ('Magnetic state', 'Magnetic state (NM, FM or AFM)', ''),
    'hform': ('Heat of formation', '', 'eV/atom'),
    'dE_NM': ('Energy relative to the NM state',
              'Energy relative to the NM state', 'meV/atom'),
    'cbm': ('CBM', 'Conduction band minimum vs vacuum (PBE)', 'eV'),
    'cbm_nosoc': ('CBM', 'Conduction band minimum vs vacuum (PBE)', 'eV'),
    'vbm': ('VBM', 'Valence band maximum vs vacuum (PBE)', 'eV'),
    'vbm_nosoc': ('VBM', 'Valence band maximum vs vacuum (PBE)', 'eV'),
    'cbm_hse': ('CBM', 'Conduction band minimum vs vacuum (HSE)', 'eV'),
    'vbm_hse': ('VBM', 'Valence band maximum vs vacuum (HSE)', 'eV'),
    'vbm_gw': ('VBM', 'Valence band maximum vs vacuum (GW)', 'eV'),
    'cbm_gw': ('CBM', 'Conduction band minimum vs vacuum (GW)', 'eV'),
    'gap': ('Band gap (PBE)', '', 'eV'),
    'gap_nosoc': ('Band gap (PBE)', '', 'eV'),
    'dir_gap': ('Direct band gap (PBE)', '', 'eV'),
    'dir_gap_nosoc': ('Direct band gap (PBE)', '', 'eV'),
    'gap_gw': ('Band gap (GW)', '', 'eV'),
    'dir_gap_gw': ('Direct band gap (GW)', '', 'eV'),
    'gap_gllbsc': ('Fundamental gap (GLLBSC)', '', 'eV'),
    'dir_gap_gllbsc': ('Direct band gap (GLLBSC)', '', 'eV'),
    'gap_hse': ('Band gap (HSE)', '', 'eV'),
    'dir_gap_hse': ('Direct band gap (HSE)', '', 'eV'),
    'hsocsplit': ('Hole spinorbit splitting', '', 'meV'),
    'esocsplit': ('Electron spinorbit splitting', '', 'meV'),
    'evacmean': ('Mean vacuum level (PBE)', '', 'eV'),
    'evac': ('Vacuum level (no dipole corr) (PBE)', '', 'eV'),
    'evacdiff': ('Vacuum level difference (PBE)', '', 'eV'),
    'excitonmass1': ('Exciton mass 1', '', '`m_e`'),
    'excitonmass2': ('Exciton mass 2', '', '`m_e`'),
    'emass1': ('Electron 1 mass 1', 'Electron 1 mass 1', '`m_e`'),
    'emass2': ('Electron 1 mass 2', 'Electron 1 mass 2', '`m_e`'),
    'hmass1': ('Hole 1 mass 1', 'Hole 1 mass 1', '`m_e`'),
    'hmass2': ('Hole 1 mass 2', 'Hole 1 mass 2', '`m_e`'),
    'emass1_nosoc': ('Electron mass 1 (no SOC)',
                     'Electron mass 1 (no SOC)',
                     '`m_e`'),
    'emass2_nosoc': ('Electron mass 1 (no SOC)',
                     'Electron mass 1 (no SOC)',
                     '`m_e`'),
    'hmass1_nosoc': ('Hole mass 1 (no SOC)',
                     'Hole mass 1 (no SOC)',
                     '`m_e`'),
    'hmass2_nosoc': ('Hole mass 2',
                     'Hole mass 2 (no SOC)',
                     '`m_e`'),
    'q2d_macro_df_slope': ('Dielectic func',
                           'Slope of macroscopic 2D static dielectric '
                           'function at q=0', '?'),
    'is_magnetic': ('Magnetic', '', ''),
    'is_dir_gap': ('Direct gap (PBE)', '', ''),
    'is_metallic': ('Metallic', '', ''),
    'maganis_zx': ('Mag. Anis. (xz)', 'Magnetic anisotropy (xz-component)',
                   'meV/formula unit'),
    'maganis_zy': ('Mag. Anis. (yz)', 'Magnetic anisotropy (yz-component)',
                   'meV/formula unit'),
    'work_function': ('Work function', '', 'eV'),
    'dosef_nosoc': ('DOS', 'Density of states at Fermi level',
                    '`\\text{eV}^{-1}`'),
    'is_dyn_stable': ('Dyn. stable', 'Dynamically stable', ''),
    'e_distortion': ('Dist. energy', 'Distortion energy per atom', 'eV'),
    'r_distortion': ('Dist. distance', 'Distortion distance change (max)',
                     'Ang'),
    'stability': ('Stability',
                  'Heat of formation and dynamic stability is tested', ''),
    'e_magnetic': ('Magn. energy', 'Energy change due to magnetic solution',
                   'eV'),
    'c_11': ('Elastic tensor (xx)',
             'Elastic tensor (xx-component)',
             'N/m'),
    'c_22': ('Elastic tensor (yy)',
             'Elastic tensor (yy-component)',
             'N/m'),
    'c_12': ('Elastic tensor (xy)',
             'Elastic tensor (xy-component)',
             'N/m'),
    'speed_of_sound_x': ('Speed of sound (x)', '', 'm/s'),
    'speed_of_sound_y': ('Speed of sound (y)', '', 'm/s'),
    'D_vbm_nosoc': ('Deformation Pot. (VBM)',
                    'Deformation potential at VBM w/o SOC',
                    'eV'),
    'D_cbm_nosoc': ('Deformation Pot. (CBM)',
                    'Deformation potential at CBM w/o SOC',
                    'eV'),
    'D_vbm': ('Deformation Pot. (VBM)',
              'Deformation potential at VBM (PBE)',
              'eV'),
    'D_cbm': ('Deformation Pot. (CBM)',
              'Deformation potential at CBM (PBE)',
              'eV'),
    'spacegroup': ('Space group',
                   'Space group',
                   ''),
    'wallpapergroup': ('Wallpaper group',
                       'Wallpaper group',
                       ''),
    'plasmafrequency_x': ('Plasma frequency (x-component)',
                          'Plasma frequency (x-component)',
                          '`\\text{eV} \\text{Ang}^{0.5}`'),
    'plasmafrequency_y': ('Plasma frequency (y-component)',
                          'Plasma frequency (y-component)',
                          '`\\text{eV} \\text{Ang}^{0.5}`'),
    'alphax': ('Static polarizability (x-component)',
               'Static polarizability (x-component)',
               'Ang'),
    'alphay': ('Static polarizability (y-component)',
               'Static polarizability (y-component)',
               'Ang'),
    'alphaz': ('Static polarizability (z-component)',
               'Static polarizability (z-component)',
               'Ang'),
    'bse_binding': ('Exciton binding energy - BSE',
                    'Exciton binding energy - BSE',
                    'eV'),
    'ehull': ('Energy above convex hull', '', 'eV/atom'),
    'minhessianeig': ('Minimum eigenvalue of Hessian', '',
                      '`\\text{eV/Ang}^2`')}

default_columns = ['formula', 'prototype', 'spacegroup', 'hform', 'gap',
                   'magstate', 'work_function']

special_keys = [
    ('SELECT', 'prototype'),
    # ('SELECT', 'class'),
    ('SELECT', 'magstate'),
    ('RANGE', 'gap', 'Band gap range [eV]',
     [('PBE', 'gap'),
      ('G0W0@PBE', 'gap_gw'),
      ('GLLBSC', 'gap_gllbsc'),
      ('HSE@PBE', 'gap_hse')])]

basic = ('Property', ['prototype', 'spacegroup', 'hform',
                      'gap', 'dir_gap', 'gap_gllbsc', 'work_function',
                      'evacdiff', 'magstate', 'dE_NM', 'ICSD_id', 'COD_id'])

# mass_vbm = ['hmass1', 'hmass2', 'hmass2_1', 'hmass2_2', 'hsocsplit']
# mass_cbm = ['emass1', 'emass2', 'emass2_1', 'emass2_2', 'esocsplit']
electronic = ['work_function', 'dosef_nosoc', 'gap', 'dir_gap', 'vbm', 'cbm']
deformation = ['D_vbm', 'D_cbm']
pbe = ('Property', (electronic + deformation))
gw = ('Property', ['gap_gw', 'dir_gap_gw', 'vbm_gw', 'cbm_gw'])
gllbsc = ('Property',
          ['dos_gllbsc', 'gap_gllbsc', 'dir_gap_gllbsc'])
hse = ('Property',
       ['work_function_hse', 'dos_hse', 'gap_hse', 'dir_gap_hse',
        'vbm_hse', 'cbm_hse'])

deformation_potential = ('Deformation Potentials (PBE)',
                         ['D_vbm', 'D_cbm'])

opt = ('Property', ['alphax', 'alphay', 'alphaz',
                    'plasmafrequency_x', 'plasmafrequency_y'])

phonons = ('Property',
           ['c_11', 'c_22', 'c_12', 'bulk_modulus',
            'minhessianeig'])

bse_binding = ('Property', ['bse_binding'])
layout = [('Basic properties',
           [[basic, 'CELL'],
            ['ATOMS']]),
          ('Stability',
           [['convex-hull.png'],
            [('Property', ['hform', 'ehull', 'minhessianeig']),
             'convex-hull.csv']]),
          ('Elastic constants and phonons',
           [['phonons.png'], [phonons]]),
          ('Magnetic properties',
           [['pbe-magnetic-properties.csv']]),
          ('Electronic band structure (PBE)',
           [['pbe-bs.png', 'bz.png'],
            ['pbe-pdos.png', pbe]]),
          ('Effective masses (PBE)',
           [['pbe-bzcut-cb-bs.png', 'pbe-bzcut-vb-bs.png'],
            ['effective-mass-cb.csv', 'effective-mass-vb.csv']]),
          ('Electronic band structure (HSE)',
           [['hse-bs.png'],
            [hse]]),
          ('Electronic band structure (GW)',
           [['gw-bs.png'],
            [gw]]),
          ('Polarizability (RPA)',
           [['rpa-pol-x.png', 'rpa-pol-z.png'],
            ['rpa-pol-y.png', opt]]),
          ('Optical absorption (BSE)',
           [['abs-in.png', bse_binding],
            ['abs-out.png']])]


data_description = {
    'bs_pbe': {
        'path': 'BZ path: (npoints, 3)-array',
        'eps_skn': 'pbe eigenvalues: (nspin, npoints, nbands)-array',
        'eps_so_mk': 'spin orbit eigenvalues: (2 x nband, npoints)-array',
        'sz_mk': 'colors: (2 x nband, npoints)-array',
        'kvbm': 'VBM k-point with SOC: (3,)-array',
        'kcbm': 'CBM k-point with SOC: (3,)-array',
        'kvbm_nosoc': 'VBM k-point without SOC: (3,)-array',
        'kcbm_nosoc': 'CBM k-point without SOC: (3,)-array'},
    'bs_gw':
        {'path': 'BZ path: str',
         'efermi': 'Fermi-level',
         'eps_skn': 'quasi-particle eigenvalues'},
    'bs_gllbsc':
        {'path': 'BZ path: (npoints, 3)-array',
         'efermi': 'Fermi-level',
         'eps_skn': 'quasi-particle eigenvalues'},
    'bs_hse':
        {'path': 'BZ path: (npoints, 3)-array',
         'kptpath': 'BZ path: str',
         'efermi': 'Fermi-level',
         'eps_skn': 'HSE eigenvalues: (nspin, npoints, nbands)-array',
         'kvbm': 'VBM k-point with SOC: (3,)-array',
         'kcbm': 'CBM k-point with SOC: (3,)-array',
         'kvbm_nosoc': 'VBM k-point without SOC: (3,)-array',
         'kcbm_nosoc': 'CBM k-point without SOC: (3,)-array'},
    'bse_pol': {'freq': 'frequency',
                'par': 'polarizability in plane',
                'per': 'polarizability out of plane'},
    'rpa_abs': {'freq': 'frequency',
                'par': 'absorbtion in plane',
                'per': 'absorbtion out of plane'},
    'rpa_pol': {'freq': 'frequency',
                'par': 'polarizability in plane'},
    'pdos_pbe_nosoc': {
        'pdos_sal': 'PDOS (nspin,)-list of dicts, \
                    i.e. pdos_sal[spin: int][a: int][l: str]',
        'energies_s': 'energies (ns, npoints)-list',
        'efermi': 'Fermi-level: float'}
}

params = {'legend.fontsize': 'large',
          'axes.labelsize': 'large',
          'axes.titlesize': 'large',
          'xtick.labelsize': 'large',
          'ytick.labelsize': 'large'}
plt.rcParams.update(**params)


def add_bs_pbe(row, ax, **kwargs):
    """plot pbe with soc on ax
   """
    c = '0.8'  # light grey for pbe with soc plot
    ls = '--'
    if 'lw' in kwargs:
        lw = kwargs['lw']
    else:
        lw = 1.5
    d = row.data.bs_pbe
    kpts = d['path']
    e_mk = d['eps_so_mk']
    xcoords, label_xcoords, labels = labels_from_kpts(kpts, row.cell)
    for e_k in e_mk[:-1]:
        ax.plot(xcoords, e_k, color=c, ls=ls, lw=lw, zorder=0)

    ef = d['efermi']
    ax.axhline(ef, ls=':', zorder=0, color=c, lw=lw)
#    ax.plot([xcoords[0], xcoords[-1]], [ef, ef], ls='-',
#            lw=lw, zorder=0, color=c)

    ax.plot(xcoords, e_mk[-1], color=c, ls=ls, lw=lw, label='PBE',
            zorder=0)
    return ax


@creates('pbe-pdos.png')
def pdos_pbe(row, filename='pbe-pdos.png', figsize=(6.4, 4.8),
             fontsize=10, lw=2, loc='best'):
    if 'pdos_pbe' not in row.data:
        return

    def smooth(y, npts=3):
        return np.convolve(y, np.ones(npts) / npts, mode='same')
    dct = row.data.pdos_pbe
    e = dct['energies']
    pdos_sal2 = dct['pdos_sal']
    z_a = set(row.numbers)
    symbols = Atoms(formula_metal(z_a)).get_chemical_symbols()

    def cmp(k):
        s, a, L = k.split(',')
        si = symbols.index(k.split(',')[1])
        li = ['s', 'p', 'd', 'f'].index(L)
        return ('{}{}{}'.format(s, si, li))
    pdos_sal = OrderedDict()
    for k in sorted(pdos_sal2.keys(), key=cmp):
        pdos_sal[k] = pdos_sal2[k]
    colors = {}
    i = 0
    for k in sorted(pdos_sal.keys(), key=cmp):
        if int(k[0]) == 0:
            colors[k[2:]] = 'C{}'.format(i)
            i += 1
    spinpol = False
    for k in pdos_sal.keys():
        if int(k[0]) == 1:
            spinpol = True
            break
    ef = dct['efermi']
    mpl.rcParams['font.size'] = fontsize
    ax = plt.figure(figsize=figsize).add_subplot(111)
    ax.figure.set_figheight(1.2 * ax.figure.get_figheight())
    emin = row.get('vbm', ef) - 3
    emax = row.get('cbm', ef) + 3
    i1, i2 = abs(e - emin).argmin(), abs(e - emax).argmin()
    pdosint_s = defaultdict(float)
    for key in sorted(pdos_sal.keys(), key=cmp):
        pdos = pdos_sal[key]
        spin, spec, lstr = key.split(',')
        spin = int(spin)
        sign = 1 if spin == 0 else -1
        pdosint_s[spin] += np.trapz(y=pdos[i1: i2], x=e[i1: i2])
        if spin == 0:
            label = '{} ({})'.format(spec, lstr)
        else:
            label = None
        ax.plot(smooth(pdos) * sign, e,
                label=label,
                color=colors[key[2:]],
                lw=lw)

    ax.legend(loc=loc)
    ax.axhline(ef, color='k', ls=':')
    ax.set_ylim(emin, emax)
    if spinpol:
        xmax = max(pdosint_s.values())
        ax.set_xlim(-xmax * 0.5, xmax * 0.5)
    else:
        ax.set_xlim(0, pdosint_s[0] * 0.5)

    xlim = ax.get_xlim()
    x0 = xlim[0] + (xlim[1] - xlim[0]) * 0.01
    text = ax.annotate('$E_\mathrm{F}$', xy=(x0, ef), ha='left', va='bottom',
                       fontsize=fontsize * 1.3)
    text.set_path_effects([path_effects.Stroke(linewidth=3,
                                               foreground='white',
                                               alpha=0.5),
                           path_effects.Normal()])
    ax.set_xlabel('projected dos [states / eV]')
    ax.set_ylabel('$E-E_\mathrm{vac}$ [eV]')
    plt.savefig(filename)
    plt.close()


def bs_xc(row, xc, **kwargs):
    """xc: 'gw' or 'hse'
    """
    from c2db.bsfitfig import bsfitfig
    lw = kwargs.get('lw', 2)
    if 'bs_' + xc not in row.data:
        return
    ax = bsfitfig(row, xc=xc, lw=lw)
    if ax is None:
        return
    label = kwargs.get('label', '?')
    ax.lines[0].set_label(label)
    if 'bs_pbe' in row.data and 'path' in row.data.bs_pbe:
        ax = add_bs_pbe(row, ax, **kwargs)
    ef = row.get('efermi_{}'.format(xc))
    ax.axhline(ef, c='k', ls=':')
    emin = row.get('vbm_' + xc, ef) - 3
    emax = row.get('cbm_' + xc, ef) + 3
    ax.set_ylabel('$E-E_\mathrm{vac}$ [eV]')
    ax.set_ylim(emin, emax)
    leg = ax.legend(loc='upper right')
    leg.get_frame().set_alpha(1)
    ax.figure.set_figheight(1.2 * ax.figure.get_figheight())
    xlim = ax.get_xlim()
    x0 = xlim[1] * 0.01
    text = ax.annotate('$E_\mathrm{F}$', xy=(x0, ef), ha='left', va='bottom',
                       fontsize=13)
    text.set_path_effects([path_effects.Stroke(linewidth=2,
                                               foreground='white',
                                               alpha=0.5),
                           path_effects.Normal()])
    figform = kwargs.get('figform', 'png')
    plt.savefig('{}-bs.{}'.format(xc, figform))
    plt.close()


@creates('gw-bs.png')
def bs_gw(row, **kwargs):
    bs_xc(row, xc='gw', label='G$_0$W$_0$', **kwargs)


@creates('hse-bs.png')
def bs_hse(row, **kwargs):
    bs_xc(row, xc='hse', label='HSE', **kwargs)


def plot_with_colors(bs, ax=None, emin=-10, emax=5, filename=None,
                     show=None, energies=None, colors=None,
                     ylabel=None, clabel='$s_z$', cmin=-1.0, cmax=1.0,
                     sortcolors=False, loc=None, s=2):
    """Plot band-structure with colors."""

    import matplotlib.pyplot as plt

    if bs.ax is None:
        ax = bs.prepare_plot(ax, emin, emax, ylabel)

    shape = energies.shape
    xcoords = np.vstack([bs.xcoords] * shape[1])
    if sortcolors:
        perm = colors.argsort(axis=None)
        energies = energies.ravel()[perm].reshape(shape)
        colors = colors.ravel()[perm].reshape(shape)
        xcoords = xcoords.ravel()[perm].reshape(shape)

    for e_k, c_k, x_k in zip(energies, colors, xcoords):
        things = ax.scatter(x_k, e_k, c=c_k, s=s,
                            vmin=cmin, vmax=cmax)

    cbar = plt.colorbar(things)
    cbar.set_label(clabel)

    bs.finish_plot(filename, show, loc)

    return ax, cbar


@creates('pbe-bzcut-cb-bs.png', 'pbe-bzcut-vb-bs.png')
def bzcut_pbe(row, figsize=(6.4, 2.8)):
    from c2db.em import evalmodel
    from ase.dft.kpoints import kpoint_convert
    sortcolors = True
    erange = 0.05  # energy window
    cb = row.get('data', {}).get('effectivemass', {}).get('cb', {})
    vb = row.get('data', {}).get('effectivemass', {}).get('vb', {})

    filename = 'pbe-bzcut-{}-bs.png'

    def getitsorted(keys, bt):
        keys = [k for k in keys if 'spin' in k and 'band' in k]
        return sorted(keys, key=lambda x: int(x.split('_')[1][4:]),
                      reverse=bt == 'vb')

    def get_xkrange(row, erange):
        xkrange = 0.0
        for bt in ['cb', 'vb']:
            xb = row.data.get('effectivemass', {}).get(bt)
            if xb is None:
                continue
            xb0 = xb[getitsorted(xb.keys(), bt)[0]]
            mass_u = abs(xb0['mass_u'])
            xkr = max((2 * mass_u * erange / Ha)**0.5 / Bohr)
            xkrange = max(xkrange, xkr)
        return xkrange

    for bt, xb in [('cb', cb), ('vb', vb)]:
        b_u = xb.get('bzcut_u')
        if b_u is None or b_u == []:
            continue

        xb0 = xb[getitsorted(xb.keys(), bt)[0]]
        mass_u = xb0['mass_u']
        coeff = xb0['c']
        ke_v = xb0['ke_v']
        fig, axes = plt.subplots(nrows=1, ncols=2, figsize=figsize,
                                 sharey=True,
                                 gridspec_kw={'width_ratios': [1, 1.25]})
        things = None
        xkrange = get_xkrange(row, erange)
        for b in b_u:  # loop over directions
            u = b['things']['u']  # direction index
            assert np.allclose(b['things']['mass'], mass_u[u])
            ut = u if bt == 'cb' else abs(u - 1)
            ax = axes[ut]
            e_mk = b['e_dft_km'].T - row.evac
            sz_mk = b['sz_dft_km'].T
            if row.get('has_invsymm', 0) == 1:
                sz_mk[:] = 0.0
            kpts_kc = b['kpts_kc']
            xk, _, _ = labels_from_kpts(kpts=kpts_kc, cell=row.cell)
            xk -= xk[-1] / 2
            # fitted model
            xkmodel = xk.copy()  # xk will be permutated
            kpts_kv = kpoint_convert(row.cell, skpts_kc=kpts_kc)
            kpts_kv *= Bohr
            emodel_k = evalmodel(kpts_kv=kpts_kv, c_p=coeff) * Ha
            emodel_k -= row.evac
            # effective mass fit
            emodel2_k = (xkmodel * Bohr) ** 2 / (2 * mass_u[u]) * Ha
            ecbm = evalmodel(ke_v, coeff) * Ha
            emodel2_k = emodel2_k + ecbm - row.evac
            # dft plot
            shape = e_mk.shape
            x_mk = np.vstack([xk] * shape[0])
            if sortcolors:
                shape = e_mk.shape
                perm = sz_mk.argsort(axis=None)
                e_mk = e_mk.ravel()[perm].reshape(shape)
                sz_mk = sz_mk.ravel()[perm].reshape(shape)
                x_mk = x_mk.ravel()[perm].reshape(shape)
            for i, (e_k, sz_k, x_k) in enumerate(zip(e_mk, sz_mk, x_mk)):
                things = ax.scatter(x_k, e_k, c=sz_k, vmin=-1, vmax=1)
            ax.set_ylabel('$E-E_\mathrm{vac}$ [eV]')
            # ax.plot(xkmodel, emodel_k, c='b', ls='-', label='3rd order')
            ax.plot(xkmodel, emodel2_k, c='r', ls='--')
            ax.set_title('Mass {}, direction {}'.format(bt.upper(), ut + 1))
            if bt == 'vb':
                y1 = ecbm - row.evac - erange * 0.75
                y2 = ecbm - row.evac + erange * 0.25
            elif bt == 'cb':
                y1 = ecbm - row.evac - erange * 0.25
                y2 = ecbm - row.evac + erange * 0.75

            ax.set_ylim(y1, y2)
            ax.set_xlim(-xkrange, xkrange)
            ax.set_xlabel('$\Delta k$ [1/$\mathrm{\AA}$]')
        if things is not None:
            cbar = fig.colorbar(things, ax=axes[1])
            cbar.set_label(r'$\langle S_z \rangle$')
            cbar.set_ticks([-1, -0.5, 0, 0.5, 1])
            cbar.update_ticks()
        fig.tight_layout()
        plt.tight_layout()
        plt.savefig(filename.format(bt))
        plt.close()


@creates('pbe-bs.png', 'pbe-vbm-bs.png', 'pbe-cbm-bs.png')
def bs_pbe(row, filename='pbe-bs.png', figsize=(6.4, 4.8),
           fontsize=10, show_legend=True, s=2):
    if 'bs_pbe' not in row.data or 'eps_so_mk' not in row.data.bs_pbe:
        return
    d = row.data.bs_pbe
    e = d['eps_skn']
    kpts = d['path']
    ef = d['efermi']
    emin = row.get('vbm', ef) - 3 - ef
    emax = row.get('cbm', ef) + 3 - ef
    mpl.rcParams['font.size'] = fontsize
    bs = BandStructure(row.cell, kpts, e, ef)
    # pbe without soc
    nosoc_style = dict(colors=['0.8'] * e.shape[0],
                       label='PBE no SOC',
                       ls='--',
                       lw=1.5,
                       zorder=0)
    ax = plt.figure(figsize=figsize).add_subplot(111)
    bs.plot(ax=ax, show=False, emin=emin, emax=emax,
            ylabel='$E-E_\mathrm{vac}$ [eV]', **nosoc_style)
    # pbe with soc
    e_mk = d['eps_so_mk']
    sz_mk = d['sz_mk']
    ax.figure.set_figheight(1.2 * ax.figure.get_figheight())
    ax, cbar = plot_with_colors(bs, ax=ax, energies=e_mk, colors=sz_mk,
                                filename=filename, show=False,
                                emin=emin, emax=emax,
                                sortcolors=True, loc='upper right',
                                clabel=r'$\langle S_z \rangle $', s=s)

    cbar.set_ticks([-1, -0.5, 0, 0.5, 1])
    cbar.update_ticks()
    csz0 = plt.get_cmap('viridis')(0.5)  # color for sz = 0
    ax.plot([], [], label='PBE', color=csz0)
    ax.set_xlabel('$k$-points')
    plt.legend(loc='upper right')
    xlim = ax.get_xlim()
    x0 = xlim[1] * 0.01
    text = ax.annotate('$E_\mathrm{F}$', xy=(x0, ef),
                       ha='left', va='bottom',
                       fontsize=fontsize * 1.3)
    text.set_path_effects([path_effects.Stroke(linewidth=2,
                                               foreground='white',
                                               alpha=0.5),
                           path_effects.Normal()])
    if not show_legend:
        ax.legend_.remove()
    plt.savefig(filename)
    if 'vbm' in row:
        ax.set_ylim(row.vbm - 0.2, row.vbm + 0.2)
        ax.figure.set_figheight(0.5 * ax.figure.get_figheight())
        plt.tight_layout()
        plt.savefig('pbe-vbm-bs.png')
    if 'cbm' in row:
        ax.set_ylim(row.cbm - 0.2, row.cbm + 0.2)
        plt.tight_layout()
        plt.savefig('pbe-cbm-bs.png')
    plt.close()


@creates('bz_nosoc.png')
def bz_nosoc(row):
    if 'bs_pbe' in row.data and 'path' in row.data.bs_pbe:
        if row.prototype in ['MoS2', 'CdI2']:
            angle = 0  # - np.pi / 3
        elif row.prototype in ['FeSe']:
            angle = 0
        elif row.prototype in ['FeOCl']:
            angle = 0
        else:
            angle = 0

        plot_bz(row, soc=False, figsize=(5, 5), sfs=1, fname='bz_nosoc.png',
                dpi=400, scale=1.5, scalecb=0.8, bbox_to_anchor=(0.5, 0.95),
                angle=angle, scbm=20, svbm=50, lwvbm=1)


@creates('bz.png')
def bz_soc(row):
    if 'bs_pbe' in row.data and 'path' in row.data.bs_pbe:
        if row.prototype in ['MoS2', 'CdI2']:
            angle = 0  # - np.pi / 3
        elif row.prototype in ['FeSe']:
            angle = 0
        elif row.prototype in ['FeOCl']:
            angle = 0
        else:
            angle = 0

        plot_bz(row, soc=True, figsize=(5, 5), sfs=1, fname='bz.png',
                dpi=400, scale=1.5, scalecb=0.8, bbox_to_anchor=(0.5, 0.95),
                angle=angle, scbm=20, svbm=50, lwvbm=1.5)


@creates('abs-in.png', 'abs-out.png')
def absorption(row):
    def xlim(delta_bse):
        return (0, 5 + delta_bse)

    def ylim(freq, data, delta_bse):
        x1, x2 = xlim(delta_bse)
        i2 = abs(freq - x2).argmin()
        return (0, data[:i2].max() * 1.02)

    def pol2abs(frequencies, pol):
        """absorption in percentage
        """
        x = 4 * np.pi * frequencies * alpha / Ha / Bohr
        return x * pol * 100

    if 'bse_pol' in row.data or 'absorptionspectrum' in row.data:
        if 'bse_pol' in row.data:
            ax = plt.figure().add_subplot(111)
            dir_gap_nosoc = row.get('dir_gap_nosoc')
            dir_gap = row.get('dir_gap')
            if dir_gap is None or dir_gap_nosoc is None:
                delta_bse = 0.0
                delta_rpa = 0.0
                dir_gap_x = None
            else:
                for method in ['_gw', '_hse', '_gllbsc', '']:
                    gapkey = 'dir_gap{}'.format(method)
                    if gapkey in row:
                        dir_gap_x = row.get(gapkey)
                        delta_bse = dir_gap_x - dir_gap
                        delta_rpa = dir_gap_x - dir_gap_nosoc
                        break
            a = row.data.bse_pol
            abs_in = pol2abs(a.freq + delta_bse, a.par)
            ax.plot(a.freq + delta_bse, abs_in, label='BSE', c='k')
            ymax2 = ylim(a.freq + delta_bse, abs_in, delta_bse)[1]
            if 'absorptionspectrum' in row.data:
                freq = row.data.absorptionspectrum.frequencies
                abs_in = pol2abs(freq + delta_rpa,
                                 row.data.absorptionspectrum.alphax_w.imag)
                ax.plot(freq + delta_rpa, abs_in, label='RPA', c='C0')
                ymin, ymax1 = ylim(freq + delta_rpa, abs_in, delta_bse)
                ymax = ymax1 if ymax1 > ymax2 else ymax2
                ax.set_ylim((ymin, ymax))
            if dir_gap_x is not None:
                ax.axvline(dir_gap_x, ls='--', c='0.5', label='Direct QP gap')
            ax.set_title('in plane')
            ax.set_xlabel('energy [eV]')
            ax.set_ylabel('absorbance [%]')
            ax.legend()
            ax.set_xlim(xlim(delta_bse))
            plt.savefig('abs-in.png')
            plt.close()
            ax = plt.figure().add_subplot(111)
            if 'bse_pol' in row.data:
                a = row.data.bse_pol
                if len(a.freq) != len(a.per):
                    plt.close()
                    return
                abs_out = pol2abs(a.freq + delta_bse, a.per)
                ax.plot(a.freq + delta_bse, abs_out, label='BSE', c='k')
                ymax2 = ylim(a.freq + delta_bse, abs_out, delta_bse)[1]
            if 'absorptionspectrum' in row.data:
                freq = row.data.absorptionspectrum.frequencies
                abs_out = pol2abs(freq + delta_rpa,
                                  row.data.absorptionspectrum.alphaz_w.imag)
                ax.plot(freq + delta_rpa, abs_out, label='RPA', c='C0')
                ymin, ymax1 = ylim(freq + delta_rpa, abs_out, delta_bse)
                ymax = ymax1 if ymax1 > ymax2 else ymax2
                ax.set_ylim((ymin, ymax))
            if dir_gap_x is not None:
                ax.axvline(dir_gap_x, ls='--', c='0.5', label='Direct QP gap')
            ax.set_title('out of plane')
            ax.set_xlabel('energy [eV]')
            ax.set_ylabel('absorbance [%]')
            ax.legend()
            ax.set_xlim(xlim(delta_bse))
            plt.tight_layout()
            plt.savefig('abs-out.png')
            plt.close()


@creates('rpa-pol-x.png', 'rpa-pol-y.png', 'rpa-pol-z.png')
def polarizability(row):
    def xlim():
        return (0, 10)

    def ylims(ws, data, wstart=0.0):
        i = abs(ws - wstart).argmin()
        x = data[i:]
        x1, x2 = x.real, x.imag
        y1 = min(x1.min(), x2.min()) * 1.02
        y2 = max(x1.max(), x2.max()) * 1.02
        return y1, y2

    if 'absorptionspectrum' in row.data:
        data = row.data['absorptionspectrum']
        frequencies = data['frequencies']
        i2 = abs(frequencies - 10.0).argmin()
        frequencies = frequencies[:i2]
        alphax_w = data['alphax_w'][:i2]
        alphay_w = data['alphay_w'][:i2]
        alphaz_w = data['alphaz_w'][:i2]

        ax = plt.figure().add_subplot(111)
        try:
            wpx = row.plasmafrequency_x
            if wpx > 0.01:
                alphaxfull_w = alphax_w - wpx**2 / (2 * np.pi *
                                                    (frequencies + 1e-9)**2)
                ax.plot(frequencies, np.real(alphaxfull_w), '--', c='C1',
                        label='real')
                ax.plot(frequencies, np.real(alphax_w), c='C1',
                        label='real interband')
            else:
                ax.plot(frequencies, np.real(alphax_w), c='C1', label='real')
        except AttributeError:
            ax.plot(frequencies, np.real(alphax_w), c='C1', label='real')
        ax.plot(frequencies, np.imag(alphax_w), c='C0', label='imag')
        ax.set_title('x-component')
        ax.set_xlabel('energy [eV]')
        ax.set_ylabel(r'polarizability [$\mathrm{\AA}$]')
        ax.set_ylim(ylims(ws=frequencies, data=alphax_w, wstart=0.5))
        ax.legend()
        ax.set_xlim(xlim())
        plt.tight_layout()
        plt.savefig('rpa-pol-x.png')
        plt.close()

        ax = plt.figure().add_subplot(111)
        try:
            wpy = row.plasmafrequency_y
            if wpy > 0.01:
                alphayfull_w = alphay_w - wpy**2 / (2 * np.pi *
                                                    (frequencies + 1e-9)**2)
                ax.plot(frequencies, np.real(alphayfull_w), '--', c='C1',
                        label='real')
                ax.plot(frequencies, np.real(alphay_w), c='C1',
                        label='real interband')
            else:
                ax.plot(frequencies, np.real(alphay_w), c='C1', label='real')
        except AttributeError:
            ax.plot(frequencies, np.real(alphay_w), c='C1', label='real')
        ax.plot(frequencies, np.imag(alphay_w), c='C0', label='imag')
        ax.set_title('y-component')
        ax.set_xlabel('energy [eV]')
        ax.set_ylabel(r'polarizability [$\mathrm{\AA}$]')
        ax.set_ylim(ylims(ws=frequencies, data=alphax_w, wstart=0.5))
        ax.legend()
        ax.set_xlim(xlim())
        plt.tight_layout()
        plt.savefig('rpa-pol-y.png')
        plt.close()

        ax = plt.figure().add_subplot(111)
        ax.plot(frequencies, np.real(alphaz_w), c='C1', label='real')
        ax.plot(frequencies, np.imag(alphaz_w), c='C0', label='imag')
        ax.set_title('z-component')
        ax.set_xlabel('energy [eV]')
        ax.set_ylabel(r'polarizability [$\mathrm{\AA}$]')
        ax.set_ylim(ylims(ws=frequencies, data=alphaz_w, wstart=0.5))
        ax.legend()
        ax.set_xlim(xlim())
        plt.tight_layout()
        plt.savefig('rpa-pol-z.png')
        plt.close()


@creates('effective-mass-vb.csv', 'effective-mass-cb.csv')
def emtable(row):
    if row.data.get('effectivemass') is None:
        return
    unit = 'm<sub>e</sub>'
    for bt in ['cb', 'vb']:
        dct = row.data.effectivemass.get(bt)
        if dct is None:
            continue
        with open('effective-mass-{}.csv'.format(bt), 'w') as f:
            if bt == 'cb':
                title = '# Electron effective mass\n'
            else:
                title = '# Hole effective mass\n'
            f.write(title)
            line = '{}, {:.2f}, {}\n'  # desc, val, unit
            keys = [k for k in dct.keys() if 'spin' in k and 'band' in k]
            for i, k in enumerate(keys):
                emdata = dct[k]
                m_u = emdata['mass_u']
                if bt == 'vb':
                    m_u *= -1
                if i == 0:
                    desc = '{}'.format(bt.upper())
                else:
                    sgn = ' + ' if bt == 'cb' else ' - '
                    desc = '{}{}{}'.format(bt.upper(), sgn, i)
                for u, m in enumerate(sorted(m_u, reverse=True)):
                    if 0.001 < m < 100:  # masses should be reasonable
                        desc1 = ', direction {}'.format(u + 1)
                        f.write(line.format(desc + desc1, m, unit))


@creates('convex-hull.png', 'convex-hull.csv')
def convex_hull(row):
    from ase.phasediagram import PhaseDiagram, parse_formula

    data = row.data.get('chdata')
    if data is None or row.data.get('references') is None:
        return

    count = row.count_atoms()
    if len(count) > 3:
        return

    refs = data['refs']
    pd = PhaseDiagram(refs, verbose=False)

    fig = plt.figure()
    ax = fig.gca()

    if len(count) == 2:
        x, e, names, hull, simplices, xlabel, ylabel = pd.plot2d2()
        for i, j in simplices:
            ax.plot(x[[i, j]], e[[i, j]], '-', color='lightblue')
        ax.plot(x, e, 's', color='C0', label='Bulk')
        dy = e.ptp() / 30
        for a, b, name in zip(x, e, names):
            ax.text(a, b - dy, name, ha='center', va='top')
        A, B = pd.symbols
        ax.set_xlabel('{}$_{{1-x}}${}$_x$'.format(A, B))
        ax.set_ylabel('$\Delta H$ [eV/atom]')
        label = '2D'
        ymin = e.min()
        for y, formula, prot, magstate, id in row.data.references:
            count = parse_formula(formula)[0]
            x = count[B] / sum(count.values())
            if id == row.id:
                ax.plot([x], [y], 'rv', label=label)
                ax.plot([x], [y], 'ko', ms=15, fillstyle='none')
            else:
                ax.plot([x], [y], 'v', color='C1', label=label)
            label = None
            ax.text(x + 0.03, y, '{}-{}'.format(prot, magstate))
            ymin = min(ymin, y)
        ax.axis(xmin=-0.1, xmax=1.1, ymin=ymin - 2.5 * dy)
    else:
        x, y, names, hull, simplices = pd.plot2d3()
        for i, j, k in simplices:
            ax.plot(x[[i, j, k, i]], y[[i, j, k, i]], '-', color='lightblue')
        ax.plot(x[hull], y[hull], 's', color='C0', label='Bulk (on hull)')
        ax.plot(x[~hull], y[~hull], 's', color='C2', label='Bulk (above hull)')
        for a, b, name in zip(x, y, names):
            ax.text(a - 0.02, b, name, ha='right', va='top')
        A, B, C = pd.symbols
        label = '2D'
        for e, formula, prot, magstate, id in row.data.references:
            count = parse_formula(formula)[0]
            x = count.get(B, 0) / sum(count.values())
            y = count.get(C, 0) / sum(count.values())
            x += y / 2
            y *= 3**0.5 / 2
            if id == row.id:
                ax.plot([x], [y], 'rv', label=label)
                ax.plot([x], [y], 'ko', ms=15, fillstyle='none')
            else:
                ax.plot([x], [y], 'v', color='C1', label=label)
            label = None
        plt.axis('off')

    plt.legend()
    plt.tight_layout()
    plt.savefig('convex-hull.png')
    plt.close()

    with open('convex-hull.csv', 'w') as f:
        f.write('# Formation energies\n')
        for e, formula, prot, magstate, id in sorted(row.data.references,
                                                     reverse=True):
            name = '{} ({}-{})'.format(formula, prot, magstate)
            if id != row.id:
                name = '<a href="/id/{}?project=c2db">{}</a>'.format(id, name)
            f.write('{}, {:.3f}, eV/atom\n'.format(name, e))


@creates('pbe-magnetic-properties.csv')
def magnetic_properties(row):
    if row.magstate == 'NM':
        return
    with open('pbe-magnetic-properties.csv', 'w') as f:
        f.write('# Property\n')
        for key in ['magstate', 'magmom', 'maganis_zx', 'maganis_zy', 'dE_NM']:
            if key in row:
                if key == 'magmom':  # reserved key is special
                    desc, unit = 'Magnetic moment', 'au'
                else:
                    _, desc, unit = key_descriptions[key]
                val = row.get(key)
                if isinstance(val, basestring):
                    line = '{}, {}, {}\n'
                elif isinstance(val, float):
                    line = '{}, {:.3f}, {}\n'
                f.write(line.format(desc, val, unit))


@creates('phonons.png')
def phonons(row):
    freqs = row.data.get('phonon_frequencies_3d')
    if freqs is None:
        return

    gamma = freqs[0]
    fig = plt.figure(figsize=(6.4, 3.9))
    ax = fig.gca()

    x0 = -0.0005  # eV
    for x, color in [(gamma[gamma < x0], 'r'),
                     (gamma[gamma >= x0], 'b')]:
        if len(x) > 0:
            markerline, _, _ = ax.stem(x * 1000, np.ones_like(x), bottom=-1,
                                       markerfmt=color + 'o',
                                       linefmt=color + '-')
            plt.setp(markerline, alpha=0.4)
    ax.set_xlabel(r'phonon frequency at $\Gamma$ [meV]')
    ax.axis(ymin=0.0, ymax=1.3)
    plt.tight_layout()
    plt.savefig('phonons.png')
    plt.close()
