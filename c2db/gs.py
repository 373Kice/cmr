from pathlib import Path

from ase.io import read
import ase.io.ulm as ulm
from gpaw import GPAW, PW, FermiDirac


def write_gpw_file():
    state = Path().cwd().parts[-1]
    name = '../relax-{}.traj'.format(state)

    u = ulm.open(name)
    params = u[-1].calculator.get('parameters', {})
    u.close()

    if not params:
        params = dict(
            mode=PW(800),
            xc='PBE',
            basis='dzp',
            kpts={'density': 6.0, 'gamma': True},
            occupations=FermiDirac(width=0.05),
            poissonsolver={'dipolelayer': 'xy'})

    slab = read(name)
    slab.pbc = (1, 1, 0)
    slab.calc = GPAW(txt='gs.txt', **params)
    slab.get_forces()
    slab.get_stress()
    slab.calc.write('gs.gpw')


if __name__ == '__main__':
    write_gpw_file()
