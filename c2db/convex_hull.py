from collections import Counter

from ase.db import connect
from ase.phasediagram import PhaseDiagram

from c2db.references import fenergy


refs = []


def convex_hull(atoms, kvp, data, verbose=False, references=None):
    if references and len(refs) == 0:
        for row in connect(references).select(u=False):
            refs.append((row.formula, row.de * row.natoms))
        assert len(refs) > 0, 'Bad file: ' + references

    N = len(atoms)
    formula = atoms.get_chemical_formula()
    energy = atoms.get_potential_energy()
    count = Counter(atoms.get_chemical_symbols())
    eform = fenergy(energy, count)

    if len(count) == 1:
        return

    try:
        pd = PhaseDiagram(refs, filter=formula, verbose=verbose)
    except ValueError:
        return

    eform0, _, _ = pd.decompose(formula)
    kvp['ehull'] = (eform - eform0) / N

    refs2 = []
    for i, (count, e, name, natoms) in enumerate(pd.references):
        refs2.append((name, pd.points[i, -1] * natoms))

    data['chdata'] = {'energy': eform,
                      'refs': refs2}
