"""

run it like:
find -name "g0w0_results.pckl" |  xargs python -m c2db.check-ks-in-gw
"""

from gpaw import GPAW
from c2db.gw import get_bandrange
from c2db.utils import eigenvalues
from c2db import chdir
import pickle
import sys
import numpy as np


folders = sys.argv[1:]

for folder in folders:
    folder = folder.replace('g0w0_results.pckl', '')
    with chdir(folder):
        try:
            with open('gap_soc.npz', 'rb') as fd:
                gap = np.load(fd)['gap']
            if gap is None:
                continue
            if not gap > 0:
                continue
            with open('g0w0_results.pckl', 'rb') as fd:
                d = pickle.load(fd, encoding='latin1')
                e_skn = d['eps']  # ks eigenvalues

            calc = GPAW('gs_gw_nowfs.gpw', txt=None)
            n1, n2 = get_bandrange(calc)
            e2_skn = eigenvalues(calc)[..., n1:n2]
            diff = e2_skn - e_skn
            maxdiff = abs(diff).max()
            if maxdiff > 1.0e-2:
                print(folder, gap, 'maxdiff in ks eigenvals:', maxdiff)
        except Exception as x:
            print(folder, x)
