from math import pi

import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from ase.dft.kpoints import (get_special_points, special_paths,
                             parse_path_string, labels_from_kpts)


def bz_vertices(cell):
    from scipy.spatial import Voronoi
    icell = np.linalg.inv(cell)
    ind = np.indices((3, 3, 3)).reshape((3, 27)) - 1
    G = np.dot(icell, ind).T
    vor = Voronoi(G)
    bz1 = []
    for vertices, points in zip(vor.ridge_vertices, vor.ridge_points):
        if -1 not in vertices and 13 in points:
            normal = G[points].sum(0)
            normal /= (normal**2).sum()**0.5
            bz1.append((vor.vertices[vertices], normal))
    return bz1


def plot(cell, paths, dots, elev=None, bbox_to_anchor=None, twod=False,
         figsize=(5, 5), angle=0):
    """
    cell: 3x3 array
    paths: [(names, points), ...]
        names = ['G', 'K', ..]
        points = [(0, 0, 0), (1/3, 1/3, 0), ...]
        plotted as lines
    dots: [(str, point, scatterplotstyle), ...]
          [('VBM', (1/3, 1/3, 0.4), {'c':'r', 'marker': 's'}), ...]
    twod: boolean
        2d bz
    """
    import matplotlib.pyplot as plt
    if twod:
        cell = np.asarray(cell).copy()
        cell[2, 2] = 1000
    # plot bz
    plt.figure(figsize=figsize)
    ax = plt.axes([0., 0.1, 0.8, 0.8], frameon=False, xticks=[], yticks=[])

    rotate = np.array([[np.cos(angle), -np.sin(angle), 0],
                       [np.sin(angle), np.cos(angle), 0],
                       [0, 0, 1]])

    bz1 = bz_vertices(cell)

    for points, normal in bz1:
        ls = '-'
        x, y, z = np.dot(rotate, np.concatenate([points, points[:1]]).T)
        ax.plot(x, y, c='0.5', ls=ls, lw=1.5, zorder=1)
    # plot paths
    txt = ''
    for names, points in paths:
        x, y, z = np.dot(rotate, np.array(points).T)
        ax.plot(x, y, c='C3', ls='--', lw=1, zorder=3)

        avg = np.sum(points, axis=0) / len(points)

        for name, point in zip(names, points):
            x, y, z = np.dot(rotate, point + (point - avg) * 0.175)
            # ha = 'left'
            # va = 'top'
            ha = 'center'
            va = 'center'
            if name == 'G':
                name = '\\Gamma'
            #     ha = 'left'
            #     va = 'bottom'
            elif len(name) > 1:
                name = name[0] + '_' + name[1]
            if len(name) > 0:
                name2 = name
                if len(name) > 1:
                    name2 = '$' + name + '$'
                text = ax.text(x, y, name2,
                               ha=ha, va=va, color='k',  # size=12,
                               zorder=5)
                import matplotlib.patheffects as path_effects
                text.set_path_effects([path_effects.Stroke(linewidth=1,
                                                           foreground='white'),
                                       path_effects.Normal()])

            txt += '`' + name + '`-'

        txt = txt[:-1] + '|'

    # plot dots
    for name, point, kwargs in dots:
        x, y, z = np.dot(rotate, point)
        if name is not None:
            kwargs['label'] = name  # name of label
        if 'zorder' not in kwargs:
            kwargs['zorder'] = 4
        ax.scatter(x, y, **kwargs)
    if dots:
        if bbox_to_anchor is not None:
            ax.legend(loc='upper center', ncol=3,
                      prop={'size': mpl.rcParams['font.size']},
                      bbox_to_anchor=bbox_to_anchor)
        else:
            ax.legend(loc='upper center', ncol=3,
                      prop={'size': mpl.rcParams['font.size']},
                      bbox_to_anchor=(0.5, 0.0))

    ax.set_aspect('equal')
    
    return ax


def get_paths(row, twod=True):
    """
    bz paths containing names of bz symmetry-points and points
    """
    cell = row.cell
    icell = np.linalg.inv(cell)
    special_points = get_special_points(cell)
    kpts = row.data.bs_pbe.path  # 'GKMG, GM'
    x, X, names = labels_from_kpts(kpts, row.cell)
    namess = [names]  # e.g. [['G','M','K','G'], ['G','M']]
    paths = []
    for names in namess:
        if twod:
            names = [name for name in names
                     if abs(special_points[name][2]) < 1.0e-10]
        points = []
        for name in names:
            k_v = special_points[name]
            k_c = np.dot(icell, k_v)
            points.append(k_c)
        paths.append((names, points))

    return paths


def plot_fermi(row, annotate=True, s=0.25, scale=None, angle=0,
               fermisurface=None):
    """
    bz paths containing names of bz symmetry-points and points
    """
    if fermisurface is not None:
        verts = fermisurface
    else:
        if 'fermisurface' not in row.data:
            return
        verts = row.data.get('fermisurface').copy()
    normalize = colors.Normalize(vmin=-1, vmax=1)
    ax = plt.gca()
    rotate = np.array([[np.cos(angle), -np.sin(angle), 0],
                       [np.sin(angle), np.cos(angle), 0],
                       [0, 0, 1]])
    verts[:, :2] /= (2 * np.pi)
    verts[:, :2] = np.dot(rotate[:2, :2], verts[:, :2].T).T
    im = ax.scatter(verts[:, 0], verts[:, 1], c=verts[:, -1],
                    s=s, cmap='viridis', marker=',',
                    norm=normalize, alpha=1, zorder=2)
    rect = np.array([0.85, 0.2, 0.025, 0.6])
    if scale is not None:
        center = np.array([0, 0.5, 0, 0])
        rect = (rect - center) * scale + center
    cbaxes = plt.gcf().add_axes(rect)
    cbar = plt.colorbar(im, cax=cbaxes, ticks=[-1, -0.5, 0, 0.5, 1])
    cbar.ax.tick_params(labelsize=mpl.rcParams['font.size'])
    cbar.set_label('$\\langle S_z \\rangle$',
                   size=mpl.rcParams['font.size'])
    if annotate:
        ax.annotate('Fermi surface', xy=(0.5, 1), ha='center',
                    va='top', xycoords='axes fraction')

    return cbaxes


def get_dots(row, soc, svbm=100, scbm=40, lwvbm=2.5):
    """
    Dots to be plotted in the BZ.

    Parameters:

    row: row object
        ase db row object

    soc: bool
        is spinorbit coupling included

    Returns:

    output: [(str, (3,) ndarray, dict), ] list
        list of on the form [(plot_label, kpt_v, plot_style), ]
    """
    cell = row.cell
    icell = np.linalg.inv(cell)
    bs = row.data.get('bs_pbe')
    if soc:
        kvbm = bs.get('kvbm')
        kcbm = bs.get('kcbm')
    else:
        kvbm = bs.get('kvbm_nosoc')
        kcbm = bs.get('kcbm_nosoc')
    dots = []
    if kvbm is not None:
        c0 = 'C3'
        c1 = 'k'
        style = {'marker': 'o', 'facecolor': 'w',
                 'edgecolors': c0, 's': svbm, 'lw': lwvbm,
                 'zorder': 5}
        dots.append(('VBM', np.dot(icell, kvbm), style))
    if kcbm is not None:
        style = {'c': c1, 'marker': 'o', 's': scbm, 'zorder': 6}
        dots.append(('CBM', np.dot(icell, kcbm), style))
    if 0:  # 'ibzk_pbe' in row.data:
        k_c = np.zeros(3)
        for j, k_c2 in enumerate(row.data['ibzk_pbe']):
            k_c[:2] = k_c2
            if j == 0:
                label = 'irr. k-points'
            else:
                label = None
            dots.append((label, np.dot(icell, k_c), {'marker': 'o',
                                                     'c': '#BBBBBB',
                                                     's': 10,
                                                     'zorder': 0}))
    return dots


def plot_ibz(row):
    if 'ibz_kc' not in row.data:
        return
    cell = row.cell
    icell = np.linalg.inv(cell)[:2, :2]
    from scipy.spatial import ConvexHull
    from matplotlib.patches import Polygon
    ibz_kv = np.dot(row.data['ibz_kc'], icell.T)
    hull = ConvexHull(ibz_kv)
    ax = plt.gca()
    ax.add_patch(Polygon(ibz_kv[hull.vertices, :], closed=True,
                         zorder=1, color=[0.8] * 3,
                         fill=True))


def plot_bz(row, soc=True, fname='bz.pdf', figsize=(6.4, 4.8),
            annotate=True, fontsize=10, svbm=100, scbm=40, lwvbm=2.5,
            sfs=0.25, dpi=200, scale=None, scalecb=None,
            bbox_to_anchor=None, angle=0):
    mpl.rcParams['font.size'] = fontsize
    paths = get_paths(row)
    dots = get_dots(row, soc=soc, svbm=svbm, scbm=scbm, lwvbm=lwvbm)
    ax = plot(row.cell, paths, dots, twod=True, figsize=figsize,
              bbox_to_anchor=bbox_to_anchor, angle=angle)
    plot_ibz(row)
    plot_fermi(row, annotate=annotate, s=sfs, scale=scalecb)
    if scale is not None:
        xlim = np.array(ax.get_xlim()) * scale
        ylim = np.array(ax.get_ylim()) * scale
        ax.set_xlim(xlim)
        ax.set_ylim(ylim)
    plt.savefig(fname, dpi=dpi)
    plt.close()


if __name__ == '__main__':
    from ase.db import connect
    con1 = connect('c2db.db')
    row = con1.get(formula='MoS2')
    plot_bz(row)
    quit()
    for X, cell in [
        ('cubic', np.eye(3)),
        # ('fcc', [[0, 1, 1], [1, 0, 1], [1, 1, 0]]),
        # ('bcc', [[-1, 1, 1], [1, -1, 1], [1, 1, -1]]),
        # ('tetragonal', [[1, 0, 0], [0, 1, 0], [0, 0, 1.3]]),
        # ('monoclinic', [[1, 0, 0], [0, 1, 0], [0, 0.2, 1]]),
        ('orthorhombic', [[1, 0, 0], [0, 1.2, 0], [0, 0, 1.4]]),
        ('hexagonal', [[1, 0, 0], [-0.5, 3**0.5 / 2, 0], [0, 0, 1]])]:
        twod = True
        icell = np.linalg.inv(cell)
        special_points = get_special_points(X, cell)
        paths = []
        for names in parse_path_string(special_paths[X]):
            points = []
            if twod:  # only use special points in the 2D BZ
                names = [name for name in names
                         if special_points[name][2] < 1.0e-10]
            for name in names:
                sp = special_points[name]
                points.append(np.dot(icell, special_points[name]))
            paths.append((names, points))

        k_vbmax = [0.333, 0.333, 0]
        k_cbmin = [0.167, 0.083, 0]
        dots = []
        dots.append(('VBM-PBE', np.dot(icell, k_vbmax),
                     {'c': 'r', 'marker': 's', 'alpha': 0.5}))
        dots.append(('CBM-PBE', np.dot(icell, k_cbmin),
                     {'c': 'b', 'marker': None, 'alpha': 0.5}))

        if X == 'bcc':
            scale = 0.6
            elev = pi / 13
        else:
            scale = 1
            elev = None

        plot(cell, paths, dots, elev, scale, twod=twod)
        plt.savefig(X + '.svg')
