from scipy.interpolate import CubicSpline
import numpy as np
from ase.dft.kpoints import labels_from_kpts
import matplotlib.pyplot as plt


def get_gw_bandrange(row):
    if 'bs_gw' in row.data:
        b = []
        for n in row.data.bs_gw.bandrange:
                b.append(2 * n)
                b.append(2 * n + 1)
        return b
    elif 'bs_hse' in row.data:
        return list(range(row.data.bs_hse.epsreal_skn.shape[-1]))
    else:
        return list(range(row.data.bs_pbe.e_so_mk.shape[0]))


def force_zero_slope(x, y, indices, derivs, tau=1.0):
    count = 0
    assert len(indices) == len(derivs)
    for i, (d1, d2) in zip(indices, derivs):
        if d1 is not None and abs(d1).max() < tau:
            j = count + i
            x = np.insert(x, j, x[j] - 1.0e-5)
            y = np.insert(y, j, y[j])
            count += 1
        if d2 is not None and abs(d2).max() < tau:
            j = count + i
            x = np.insert(x, j + 1, x[j] + 1.0e-5)
            y = np.insert(y, j + 1, y[j])
            count += 1
    return x, y


def Xindices2(x, X, tau=1.0e-5):
    """X = x[indices]
    """
    indices = []  # indices of high symm points
    for Xi in X:
        diff = abs(x - Xi)
        i = diff.argmin()
        if diff[i] < tau:
            indices.append(i)
    return indices


def Xindices(kpts, cell):
    """X = x[indices]
    """
    x, X, labels = labels_from_kpts(kpts=kpts, cell=cell)
    indices = Xindices2(x=x, X=X)
    return indices, x, X, labels


def get_pbe_ind_val_derivs(row):
    """derivaties at the high symmetry points from left and right
    """
    d = row.data.bs_pbe
    indices, x, _, _ = Xindices(kpts=d['path'], cell=row.cell)
    epbe_mk = d.eps_so_mk[get_gw_bandrange(row)]
    # derivatives
    Dpbe_km = (np.diff(epbe_mk, axis=1) / np.diff(x)).T
    derivs_Xm = []
    derivs_Xm.append([None, Dpbe_km[0]])
    for i in indices[1:-1]:
        derivs_Xm.append([Dpbe_km[i - 1], Dpbe_km[i]])
    derivs_Xm.append([Dpbe_km[-1], None])
    return indices, derivs_Xm, epbe_mk.T


def bsfitfig(row, xc, **kwargs):
    d = row.data.get('bs_' + xc)
    if 'xkreal' not in d or 'epsreal_skn' not in d:
        return
    indices2, ds_Xm, e2_km = get_pbe_ind_val_derivs(row)
    d2 = row.data.bs_pbe
    _, X, labels = labels_from_kpts(kpts=d['path'], cell=row.cell)
    xfit, Xfit, labelsfit = labels_from_kpts(kpts=d2['path'], cell=row.cell)
    x = d['xkreal']
    e_km = d['epsreal_skn'][0]
    indices = Xindices2(X=X, x=x)

    sp_m = []
    for e_k in e_km.T:
        xt, yt = force_zero_slope(x=x, y=e_k, indices=indices, derivs=ds_Xm)
        sp_m.append(CubicSpline(x=xt, y=yt))

    fig = plt.figure()
    ax = fig.add_subplot(111)
    for sp in sp_m:
        ax.plot(xfit, sp(xfit), c='k', ls='-', **kwargs)

    for e_k in e_km.T:
        ax.plot(x, e_k, 'o', fillstyle='full', c='C0', zorder=-10,
                markersize=kwargs.get('ms', 5))
    ax.set_xlim(x[0], x[-1])
    ax.set_xticks(X)
    ax.set_xticklabels([x.replace('G', '$\Gamma$') for x in labels])
    for Xi in X:
        ax.axvline(Xi, ls='-', c='0.5', zorder=-20)
    return ax
