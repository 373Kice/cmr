import os.path as op

import numpy as np

from ase.dft.bandgap import bandgap
from ase.dft.kpoints import kpoint_convert
from ase.parallel import world
from ase.units import Hartree, Bohr

from gpaw import GPAW

from c2db.em import em
from c2db.em import kptsincircle
from c2db.utils import gpw2eigs


def eh(gpw):
    e_km, efermi = gpw2eigs(gpw, soc=True)
    gap, (k1, m1), (k2, m2) = bandgap(eigenvalues=e_km, efermi=efermi)
    if not gap > 0:
        return
    e1_k = e_km[:, m1]
    e2_k = e_km[:, m2]
    de_k = e2_k - e1_k
    return de_k


def emexciton(gpw='pdos.gpw'):
    calc = GPAW(gpw, txt=None)
    kpts_kc = calc.get_ibz_k_points()
    cell = calc.atoms.get_cell()
    eh_k = eh(gpw)
    if eh_k is None:
        return
    gpw2 = 'em_exciton_soc.gpw'
    if not op.isfile(gpw2):
        kmin = eh_k.argmin()
        ke_c = kpts_kc[kmin]
        kpts_kc = kptsincircle(cell)
        calc.set(kpts=kpts_kc + ke_c, txt='em_exciton_soc.txt', symmetry='off')
        calc.get_potential_energy()
        calc.write(gpw2)

    calc = GPAW(gpw2, txt=None)
    kpts_kc = calc.get_ibz_k_points()
    kpts_kv = kpoint_convert(skpts_kc=kpts_kc, cell_cv=cell)
    eh2_k = eh(gpw2)
    d = em(kpts_kv=kpts_kv * Bohr, eps_k=eh2_k / Hartree, bandtype='cb')
    if world.rank == 0:
        np.savez('em_exciton_soc.npz', **d)


if __name__ == '__main__':
    emexciton()
