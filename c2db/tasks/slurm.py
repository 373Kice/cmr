import os
from pathlib import Path
import subprocess

from c2db.tasks.tasks import command


class SLURM:
    def submit(self, task, jobids):
        for size in [24, 16, 8]:
            if task.cores % size == 0:
                nodes = task.cores // size
                break
        else:
            if task.cores == 1:
                size = 8
                nodes = 1
            else:
                1 / 0

        cmd = ['sbatch',
               '--partition=xeon{}'.format(size),
               '--job-name={}'.format(task.name),
               '--mem=0',
               '--time={}'.format(task.time // 60),
               '--ntasks={}'.format(task.cores),
               '--nodes={}'.format(nodes),
               '--output=slurm-{}-%j.out'.format(task.name),
               '--error=slurm-{}-%j.err'.format(task.name)]

        if jobids:
            ids = ':'.join(str(id) for id in jobids)
            cmd.append('--dependency=afterok:{}'.format(ids))

        mpi = 'mpirun'
        if size == 24:
            mpi += ' -mca pml cm -mca mtl psm2 -x OMP_NUM_THREADS=1'

        args = command(task.module, task.function)

        script = ('#!/bin/bash -l\n'
                  'python3 -m c2db.tasks clear -s running {name} . && '
                  '{mpi} gpaw-python {args} && '
                  'python3 -m c2db.tasks clear -s done {name} .\n'
                  .format(mpi=mpi, args=args, name=task.name))

        p = subprocess.Popen(cmd,
                             stdin=subprocess.PIPE,
                             stdout=subprocess.PIPE)
        out, err = p.communicate(script.encode())
        assert p.returncode == 0
        jobid = int(out.split()[-1])
        return jobid

    def timeout(self, name, id):
        err = Path('slurm-{}-{}.err'.format(name, id))
        if err.is_file():
            with open(str(err)) as f:
                for line in f:
                    if line.endswith('DUE TO TIME LIMIT ***\n'):
                        return True
        return False

    def cancel(self, ids):
        subprocess.run(['scancel'] + [str[id] for id in ids])

    def jobs(self):
        user = os.environ['USER']
        cmd = ['squeue', '--user', user]
        try:
            p = subprocess.run(cmd, stdout=subprocess.PIPE)
        except FileNotFoundError:
            cmd[:0] = ['ssh', os.environ.get('SLURM_FRONTEND', 'sylg')]
            p = subprocess.run(cmd, stdout=subprocess.PIPE)
        queued = {int(line.split()[0]) for line in p.stdout.splitlines()[1:]}
        return queued
