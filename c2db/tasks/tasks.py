import os
from pathlib import Path
import subprocess
import time

from ase.utils import Lock

from c2db import chdir


taskstates = ['todo', 'queued', 'running', 'done', 'FAILED', 'TIMEOUT']


class TasksError(Exception):
    pass


def T(t):
    """Convert string to seconds."""
    if isinstance(t, str):
        t = {'m': 60, 'h': 3600, 'd': 24 * 3600}[t[-1]] * int(t[:-1])
    return t


class Task:
    def __init__(self, name, deps=[], cores=1, time='1m'):
        self.module, _, self.function = name.partition(':')

        self.name = name
        self.deps = deps
        self.cores = cores
        self.time = T(time)

        self.state = 'unknown'  # todo, queued, running, done, FAILED, TIMEOUT
        self.jobid = 0

    def __str__(self):
        s = '{:20} {}'.format(self.name, self.state)
        if self.jobid:
            s += ' ' + str(self.jobid)
        return s

    def submit(self, queue):
        if self.state == 'todo':
            ids = []
            for dep in self.deps:
                if dep.state in {'FAILED', 'TIMEOUT'}:
                    return 0
                id = dep.submit(queue)
                if id:
                    ids.append(id)

            self.jobid = queue.submit(self, ids)
            print('Submitted:', self, ids)
            append(self.name, 'queued', self.jobid)
            self.state = 'queued'

        return self.jobid


class Tasks:
    def __init__(self, queue):
        self.queue = queue
        self.tasks = []
        self.flags = set()

    def add(self, name, resources=None, deps=[], cores=1, time='1m'):
        if resources is not None:
            cores, time = resources.split('x')
            cores = int(cores)

        task = Task(name, deps, cores, time)
        self.tasks.append(task)
        return task

    def update(self, queued):
        status = self.read_status()
        for task in self.tasks:
            state, jobid = status.get(task.name, ('todo', 0))
            if jobid not in queued:
                if state == 'running':
                    if self.queue.timeout(task.name, jobid):
                        state = 'TIMEOUT'
                    else:
                        state = 'FAILED'
                if state == 'queued':
                    state = 'todo'
                jobid = 0
            task.state = state
            task.jobid = jobid

    def submit(self):
        for task in self.tasks:
            task.submit(self.queue)

    def filter(self, name):
        self.tasks = [task for task in self.tasks if task.name == name]

    def read_status(self):
        status = {}
        if os.path.isfile('.tasks'):
            with open('.tasks') as f:
                for line in f:
                    words = line.split()
                    t, state, name = words[:3]
                    name = name.replace('c2dm', 'c2db')
                    if len(words) == 4:
                        id = int(words[3])
                    else:
                        id = status.get(name, ('', 0))[1]
                    status[name] = (state, id)
        return status

    def cancel(self):
        self.queue.cancel([id for name, id in self.submitted()])
        if os.path.isfile('submitted.tasks'):
            os.remove('submitted.tasks')


def append(name, state, id=0):
    with Lock('tasks.lock'):
        with open('.tasks', 'a') as f:
            f.write('{}  {:10}{}{}\n'
                    .format(time.strftime('%Y-%m-%d-%H:%M:%S'),
                            state,
                            name,
                            '' if id == 0 else '  {}'.format(id)))


def find_setup_function(name='tasks.py'):
    here = Path().cwd()
    while True:
        filename = here / name
        if filename.is_file():
            dct = {}
            with open(str(filename)) as f:
                exec(compile(f.read(), str(filename), 'exec'), dct)
            return dct['setup']
        parent = here.parent
        if parent == here:
            raise TasksError('Could not find your workflow.py file.')
        here = parent


def folders(names, glob=False):
    """Generate paths to all folders."""
    if names:
        for name in names:
            dir = Path(name)
            if dir.is_dir():
                yield dir
    elif glob:
        yield from Path('.').glob('**/')


def command(module, function=None):
    """Command for running function() in module."""
    if function:
        return '-c "import {}; {}.{}()"'.format(module, module, function)
    return '-m ' + module


def main():
    import argparse
    parser = argparse.ArgumentParser(
        prog='tasks',
        description='Submit trees of tasks to queueing system.')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='More output.')
    parser.add_argument('--flags', metavar='FLAG1,FLAG2,..',
                        help='Set tasks.flags for use in workflow.py.')
    parser.add_argument('-w', '--workflow', default='workflow.py',
                        help='Work-flow description file.  Default: '
                        'workflow.py.')

    subparsers = parser.add_subparsers(dest='command')

    help = 'List tasks in queue and tasks that can be submitted.'
    list_parser = subparsers.add_parser('list',
                                        description=help,
                                        help=help)
    default = ','.join(s[0] for s in taskstates)
    list_parser.add_argument('-s', '--states', metavar='STATE1,STATE2,...',
                             default=default,
                             help='Comma-separated list of states to show. '
                             'Possible states: "{}".  First letter '
                             'also works: -s F,T (same as -s FAILED,TIMEOUT). '
                             'Default is {}.'
                             .format('", "'.join(taskstates), default))

    help = 'Put available tasks in queue.'
    submit_parser = subparsers.add_parser('submit',
                                          description=help,
                                          help=help)
    submit_parser.add_argument('-o', '--only', metavar='TASK',
                               help='Only submit task named "TASK".')

    help = 'Run task(s) locally.'
    run_parser = subparsers.add_parser('run',
                                       description=help,
                                       help=help)
    run_parser.add_argument('-0', '--dry-run', action='store_true',
                            help='List task instead of running them.')
    run_parser.add_argument('task', help='Name of task to run.')

    help = 'Clear status for task(s).'
    clear_parser = subparsers.add_parser('clear',
                                         description=help,
                                         help=help)
    clear_parser.add_argument('-s', '--status', default='todo',
                              help='New status.  Defaults to "todo".')
    clear_parser.add_argument('task', help='Name of task to clear.')

    list_parser.add_argument('folder', nargs='*',
                             help='List of folders.  Defaults to current '
                             'folder and its folders and its folders and ...')

    for p in [submit_parser, run_parser, clear_parser]:
        p.add_argument('folder', nargs='+',
                       help='List of folders.')

    args = parser.parse_args()

    if args.command == 'clear':
        for folder in folders(args.folder):
            with chdir(folder):
                append(args.task, args.status)
        return

    if args.command == 'run':
        name = args.task
        module, _, function = name.partition(':')
        for folder in folders(args.folder):
            with chdir(folder):
                cmd = 'python3 ' + command(module, function)
                if args.dry_run:
                    print(folder, cmd)
                else:
                    print(folder)
                    err = subprocess.call(cmd, shell=True)
                    if err:
                        append(name, 'FAILED')
                        break
                    append(name, 'done')
        return

    setup = find_setup_function(args.workflow)

    from c2db.tasks.slurm import SLURM
    queue = SLURM()
    queued = queue.jobs()

    if args.command == 'list':
        glob = True
        states = set()
        for s in args.states.split(','):
            for state in taskstates:
                if s == state or s == state[0]:
                    break
            else:
                raise ValueError('Unknown state: ' + s)
            states.add(state)
    else:
        glob = False

    for folder in folders(args.folder, glob=glob):
        tasks = Tasks(queue)
        if args.flags:
            tasks.flags = args.flags.split(',')
        with chdir(folder):
            setup(tasks)
            if args.command == 'submit' and args.only:
                tasks.filter(args.only)
            tasks.update(queued)
            if args.command == 'list':
                for task in tasks.tasks:
                    if task.state in states:
                        print('{!s:20} {}'.format(folder, task))
            else:
                tasks.submit()
