import importlib
import os
from pathlib import Path
import time
from functools import partial
from multiprocessing import Process
from ase.utils import Lock

from c2db.tasks.tasks import append


class TEST:
    def __init__(self):
        self.dir = os.getcwd()

    def submit(self, task, jobids):
        id = self.write('submitted')
        p = Process(target=self.run, args=(task, id, jobids))
        p.demon = True
        p.start()
        return id

    def run(self, task, id, jobids):
        time.sleep(0.2)
        if jobids:
            self.write('dependency', id)
            while True:
                nextid, queue = self.read()
                if all(jobid not in queue for jobid in jobids):
                    break
                time.sleep(0.2)
            self.write('waiting', id)
        else:
            self.write('waiting', id)
        time.sleep(2.4)
        append(task.name, 'running')
        self.write('running', id)
        mod, _, func = task.name.partition(':')
        mod = importlib.import_module(mod)
        if func:
            getattr(mod, func)()
        append(task.name, 'done')
        self.write('done', id)

    def jobs(self):
        return {id for id, status in self.read()[1].items()
                if status != 'done'}

    def read(self):
        with Lock(self.dir + '/slurm.lock'):
            if not os.path.isfile(self.dir + '/slurm.queue'):
                return 1, {}
            with open(self.dir + '/slurm.queue') as f:
                n = int(f.readline())
                return n, {int(id): status
                           for id, status in (line.split() for line in f)}

    def write(self, status, id=None):
        nextid, queue = self.read()
        if id is None:
            id = nextid
            nextid += 1

        if status == 'done':
            del queue[id]
        else:
            queue[id] = status

        with Lock(self.dir + '/slurm.lock'):
            with open(self.dir + '/slurm.queue', 'w') as f:
                print(nextid, file=f)
                for id, status in sorted(queue.items()):
                    print(id, status, file=f)

        return id


def setup(t):
    t1 = t.add('c2db.jobs:t1')
    t2 = t.add('c2db.jobs:t2', deps=[t1])
    if 'slow' in t.flags:
        t.add('c2db.jobs:t3', [t1], cores=24, time='2d')
    if something():
        t.add('c2db.jobs:t4', [t2])


def something():
    return os.path.isfile('t1.something')


def task(*paths):
    for path in paths:
        Path(path).touch()


t1 = partial(task, 't1.out', 't1.something')
t2 = partial(task, 't2.out')
t3 = partial(task, 't3.out')
t4 = partial(task, 't4.out')
