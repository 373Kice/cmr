import argparse
from ase.build import niggli_reduce
from ase.io import Trajectory, read
from c2db import chdir, magnetic_atoms, readinfo, ensure_dir, update_info
from pathlib import Path


def get_prototype(name):
    return (name.split('-')[1]).split('/')[0]


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Create afm protoptype')
    parser.add_argument('folder', nargs='+')
    parser.add_argument('-v', '--verbose', action='store_true')
    parser.add_argument('-p', '--prototype', default='')
    parser.add_argument('-c', '--cls', default='')
    args = parser.parse_args()
    folders = [f.rstrip('/') for f in args.folder]
    for folder in folders:
        folder_afm = folder + '-afm'
        if not Path(folder).is_dir():
            continue
        else:
            with chdir(folder):
                info = readinfo()
        if Path(folder + '/' + 'relax-afm.traj').is_file():
            with chdir(folder):
                atoms = read('relax-afm.traj')
        elif Path(folder + '/' + 'relax-fm.traj').is_file():
            fname = 'relax-fm.traj'
            with chdir(folder):
                try:
                    atoms = read(fname)
                except OSError as e:
                    print(folder + ':', e)
                ma = magnetic_atoms(atoms)
                if sum(ma) == 1:
                    atoms = atoms.repeat((1, 2, 1))
                    atoms.pbc = (True, True, True)
                    niggli_reduce(atoms)
                    atoms.pbc = (True, True, True)
                else:
                    continue
        ensure_dir(folder_afm)
        with chdir(folder_afm):
            info_afm = {'class': info.get('class', ''),
                        'prototype': info.get('prototype', '') + '-afm',
                        'gs': 'afm',
                        'states': ['afm']}
            if args.prototype != '':
                info_afm['prototype'] = args.prototype
            if args.cls != '':
                info_afm['class'] = args.cls
            update_info(**info_afm)
            if not Path('relax-afm.traj').is_file():
                traj = Trajectory('start.relax-afm.traj', mode='a',
                                  atoms=atoms)
                traj.write()
