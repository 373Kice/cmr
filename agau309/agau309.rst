.. _agau309:

=========================================================
Ground state orderings in icosahedral Ag-Au nanoparticles
=========================================================

Structures at every concentration of a 309-atom Mackay icosahedron,
obtained using exact solution of a cluster expansion model.

.. container:: article

    Rich Ground-State Chemical Ordering in Nanoparticles:
    Exact Solution of a Model for Ag-Au Clusters

    Peter Mahler Larsen, Karsten Wedel Jacobsen, and Jakob Schiøtz

    PHYSICAL REVIEW LETTERS, In press


* Download data: :download:`agau309.db`
* `Browse data <https://cmrdb.fysik.dtu.dk/?project=agau309>`_
